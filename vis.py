import numpy as np
import cv2
import colour_demosaicing
import matplotlib.pyplot as plt

path = "data/pred/sim/20201026_145919862_flare_input.npy"
BL = 256
WL = 4096

img = np.load(path)
print(img.shape, img.min(), img.max())
img = img / img.max() * 16
rgb = colour_demosaicing.demosaicing_CFA_Bayer_Menon2007(img, pattern="BGGR")
print(rgb.shape, rgb.min(), rgb.max())
plt.imshow(rgb)
plt.show()
