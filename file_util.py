import os
from colour_demosaicing import demosaicing_CFA_Bayer_Menon2007
import numpy as np
import cv2
import glob
import struct
import imageio
from functools import cmp_to_key
import torch

import sys
sys.path.append('.')

# deps for standarized dump
from simple_ISP import simpleCFA, visualizeRaw, simpleISP, loadLUT
from utils import normalize_img, torch_to_np

def dump(data, output_folder, module_name='', posfix='',
              normalize=False, black_level=256.0, white_level=4095.0, 
              clip=False, inv_gamma=1, inv_ltm=True, visualization='bayer', 
              swap_RGB=False, save_npy=True, save_raw=True, add_raw_blacklevel=False, bpp=10):
    ############################
    # auto adjust the settings #
    ############################
    # calculate the input range (can't always retrived from dtype)
    # print(posfix, data.max())
    if isinstance(data, torch.Tensor):
        data = torch_to_np(data)
    
    input_max = data.max()
    input_min = data.min()
    bits = [0, 8, 10, 12, 16]
    input_bit = -1;
    for bit in bits:
        if input_max // 2**(bit+1) < 1:
            input_bit = bit;
            break;
    print(input_min, input_max)
    # if input is [0-1] float, unnormalize it and add black level back.
    if input_bit == 0:
        if input_max == input_min:
            pass # skip for all black input data
        if bpp == 10:
            data = data * (1023.0 - 64.0) + 64.0
        if bpp == 12:
            data = data * (4095.0 - 256.0) + 256.0
        normalize = True
        add_raw_blacklevel = False
    if input_bit == 10 or input_bit == 12:
        normalize = True
    print(data.min(), data.max())
        
    # if input is > 10bit, need to clip the result for visualization, but unadjusted for saving raw
    data_vis = data
    if input_bit > bpp:
        data_vis = np.clip(data, None, 2**bpp - 1)

    # additional input from user for adding back blacklevel. This will be skipped if input is [0,1] float
    if add_raw_blacklevel:
        if bpp == 10:
            data = data / 1023.0 * (1023.0 - 64.0) + 64.0
        if bpp == 12:
            data = data / 4095.0 * (4095.0 - 256.0) + 256.0
    print(data.min(), data.max())
    # print('after auto adjust', data_vis.max(), data.max())
    
    # when using simple ISP, we don't need to clip/brighten the image before the process; 
    # but we will always need to swap RGB channel for correct visualization
    if visualization == 'simpleISP':
        clip = False 
        inv_gamma = 1.0
        swap_RGB = True

    if visualization == 'halfres':
        swap_RGB = True
    
    # only save the bayer .raw file, not support for RGB input
    if data.ndim == 3:
        save_raw = False 
    
    ################
    # do visualize #
    ################
    dump_core(data_vis, output_folder, module_name, posfix,
              normalize, black_level, white_level, 
              clip, inv_gamma, inv_ltm, visualization, 
              swap_RGB, False, False, bpp)  
    
    ###########
    # do save #
    ###########
    dump_core(data, output_folder, module_name, posfix,
              normalize, black_level, white_level, 
              clip, inv_gamma, inv_ltm, 'none', 
              swap_RGB, save_npy, save_raw, bpp)  

"""
Standarized dump method that visualized input/output, saves data in npy and raw.
"""
def dump_core(data, output_folder, module_name='', posfix='',
         normalize=False, black_level=64.0, white_level=1023.0, 
         clip=False, inv_gamma=1, inv_ltm=True, visualization='bayer', 
         swap_RGB=False, save_npy=True, save_raw=True, bpp=12):
    """
    :param data: (cv_Mat) data to be visualized and dumped
    :param output_folder: (string) output subdirectory, _result/<output_folder>/
    :param module_name: (string) process module name, ['bpc', 'mfnr', ...]
    :param posfix: (string) name of the dumped data
    :param normalize: (bool) normalize image to [0,1], default no normalize
    :param black_level: (int) black level of the input
    :param white_level: (int) black level of the input
    :param visualization: (string) visualzied input output resolution ['bayer', 'halfres', 'fullres', 'gray', 'simpleISP', none]
    :param swap_RGB: (bool) swap the color channel from bgr to rgb for correct visualization
    :param save_npy: (bool) save npy form of output, default always dump
    :param save_raw: (bool) save raw form of output, default always dump
    :param bpp: (int) bit-per-pixel of the data range
    """

    if save_npy is True:
        np.save(os.path.join(output_folder, module_name + posfix + '.npy'), data)
    print(f"dump {visualization}", data.min(), data.max())
    if normalize is True: 
        data = normalize_img(data, black_level, white_level)
    print(data.min(), data.max())
    if clip is True:
        data = np.clip(data, 1e-6, 1)
    
    if inv_gamma != 1:
        data = np.power(data, 1.0 / inv_gamma)

    if inv_ltm:
        mu =1000
        data=(np.exp(np.log(1+mu)*np.clip(data,0,1))-1)/mu
    
    print(data.min(), data.max())
    
    if data.max() <= 2 and visualization != 'simpleISP':
        data *= 255

    if visualization == 'bayer':
        pass
    if visualization == 'gray':
        data = (data[::2, ::2] + data[1::2, ::2] + data[::2, 1::2] + data[1::2, 1::2]) / 4.0
    elif visualization == 'halfres':
        H, W = data.shape
        # assume BGGR
        rgb = np.zeros((int(H//2), int(W//2), 3), dtype=np.float32)
        rgb[:,:,0] = np.float32(data[1::2, 1::2])
        rgb[:,:,1] = np.float32((data[::2, 1::2] + data[1::2, ::2])/2.0)
        rgb[:,:,2] = np.float32(data[::2, ::2])
        data = rgb
    elif visualization == 'simpleISP':
        imgCFA = data * white_level
        print("cfa", imgCFA.min(), imgCFA.max()) 
        #imgCFA = simpleCFA(data * white_level)
        DRCGain, DRCInvGain, GammaLUT, GammaInvLUT, CCM, invCCM = loadLUT(bpp)
        _, _, imgRGB = simpleISP(imgCFA, bpp, DRCGain, GammaLUT, CCM, False, 0, '')
        print("rgb", imgRGB.min(), imgRGB.max())
        data = np.uint8(imgRGB)
    elif visualization == 'fullres':
        # todo
        data = demosaicing_CFA_Bayer_Menon2007(data, pattern='BGGR')
        pass
    elif visualization == 'none':
        return

    if swap_RGB:
        data = cv2.cvtColor(np.uint8(data), cv2.COLOR_BGR2RGB)

    cv2.imwrite(os.path.join(output_folder, module_name + posfix + '.png'), data)


