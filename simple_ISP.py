import numpy as np
import cv2
import os
from scipy import interpolate
import sys
from utils import normalize_img

DRC = [[0, 0], [16, 8147], [32, 12638], [64, 11155], [128, 9327], [192, 8296], [256, 7626], [320, 7150], [384, 6792], [512, 6284], [640, 5938], [768, 5684], [896, 5489], [1024, 5334], [1280, 5102], [1536, 4936],
[1792, 4809], [2048, 4710], [2560, 4564], [3072, 4460], [3584, 4382], [4096, 4321], [4608, 4272], [5120, 4232], [6144, 4170], [7168, 4123], [8192, 4096], [9216, 4089], [10240, 4085], [11264, 4083],
[13312, 4085], [15360, 4091], [16384, 4096]]

Gamma = [[0, 0], [64, 221], [128, 442], [256, 885], [384, 1328], [512, 1771], [640, 2231], [768, 2674], [896, 3116], [1024, 3576], [1280, 4422], [1536, 5250], [1792, 5950], [2048, 6600], [2304, 7200],
[2560, 7750], [2816, 8250], [3072, 8750], [3584, 9550], [4096, 10280], [4608, 10950], [5120, 11500], [6144, 12430], [7168, 13200], [8192, 13850], [9216, 14440], [10240, 14880], [11264, 15280],
[12288, 15600], [13312, 15850], [14336, 16080], [16384, 16320]]

CCMB = [[1.685547, -0.501953, -0.183594], [-0.207031, 1.369141, -0.162109], [0.021484, -0.642578, 1.621094]]

def loadLUT(bpp):
    """
        load corresponding LUT
        :param bpp: bit per pixel of img
        :return DRCGain: DRC gain LUT index: 0~2**bpp-1, output: gain larger than 1.0
        :return GammaLUT: Gamma LUT index: 0~2**bpp-1, output: 0~255
        :return CCM: CCM matrix 3x3
    """
    maxRange = 2**bpp
    DRCX = np.array([int(x) for [x,y] in DRC]).astype(np.float32)/16384*(maxRange-1)
    DRCY = np.array([int(y) for [x,y] in DRC]).astype(np.float32)/4096.0
    fDRC = interpolate.PchipInterpolator(DRCX, DRCY, extrapolate=True)
    DRCGain = fDRC(np.arange(maxRange)).astype(np.float32)
    # backward DRCLUT
    DRCX = DRCX*DRCY
    DRCY = 1/np.maximum(1, DRCY)
    DRCY[0] = 0
    fDRC = interpolate.PchipInterpolator(DRCX, DRCY, extrapolate=True)
    DRCInvGain = fDRC(np.arange(maxRange)).astype(np.float32)

    GammaX = np.array([int(x) for [x,y] in Gamma]).astype(np.float32)/16384*(maxRange-1)
    GammaY = np.array([int(y) for [x,y] in Gamma]).astype(np.float32)/64
    fGamma = interpolate.PchipInterpolator(GammaX, GammaY, extrapolate=True)
    GammaLUT = fGamma(np.arange(maxRange)).astype(np.float32)
    # backward GammaLUT
    fGamma = interpolate.PchipInterpolator(GammaY, GammaX, extrapolate=True)
    GammaInvLUT = fGamma(np.arange(maxRange)/(maxRange-1)*255).astype(np.float32)

    CCM = np.array(CCMB).astype(np.float32)
    invCCM = np.linalg.inv(CCM)

    return DRCGain, DRCInvGain, GammaLUT, GammaInvLUT, CCM, invCCM

def visualizeRaw(img, bpp, resize=False, color=False):
    """
        visualize raw data
        :param img: could be 3D or 2D, float or uint8 or uint16
        :param bpp: bit per pixel of img
        :param resize: if true, resize image to resolution around 400
        :param color: if true, perform color conversion
        :return imgVis: image good to be viewed using cv2.imshow
    """

    imgVis = np.clip(img / (2 ** bpp - 1) * 255, a_min=0, a_max=255).astype(np.uint8)
    if resize:
        H, W = img.shape[0], img.shape[1]
        ratio = np.round(W/450)
        Hn, Wn = int(H//ratio), int(W//ratio)
        imgVis = cv2.resize(imgVis, (Wn, Hn), interpolation=cv2.INTER_LINEAR)
    if color:
        imgVis = cv2.cvtColor(imgVis, cv2.COLOR_BGR2RGB)
    return imgVis

def rgb2yuv(img):
    """
        convert rgb image to yuv image
        :param img: a 3D numpy float32 0~255 HxWx3
        :return yuv: a 3D numpy float32 0~255 HxWx3
    """
    yuv = np.zeros_like(img)
    r, g, b = img[:,:,0], img[:,:,1], img[:,:,2]
    yuv[:, :, 0] = np.clip(np.round((77 * r + 150 * g + 29 * b) / 256), a_min=0, a_max=255)
    yuv[:, :, 1] = np.clip(np.round((-43 * r - 85 * g + 128 * b) / 256) + 128, a_min=0, a_max=255)
    yuv[:, :, 2] = np.clip(np.round((128 * r - 107 * g - 21 * b) / 256) + 128, a_min=0, a_max=255)
    return yuv

def yuv420(img):
    """
        convert yuv image to ycbcr
        :param img: a 3D numpy float32 0~255 HxWx3
        :return outY: a 2D numpy float32 0~255 HxW
        :return outC: a 2D numpy float32 0~255 H/2*w
    """
    H, W, _ = img.shape
    outY = img[:,:,0].copy()
    outC = np.zeros((H//2, W))
    outCb = (img[::2, ::2, 1] + img[::2, 1::2, 1] + img[1::2, ::2, 1] + img[1::2, 1::2, 1])/4
    outCr = (img[::2, ::2, 2] + img[::2, 1::2, 2] + img[1::2, ::2, 2] + img[1::2, 1::2, 2])/4
    outCb = np.clip(np.round(outCb), a_min=0, a_max=255)
    outCr = np.clip(np.round(outCr), a_min=0, a_max=255)
    outC[:,::2] = outCr
    outC[:,1::2] = outCb
    return outY, outC

def yuv2rgb(imgY, imgC):
    H, W = imgY.shape
    imgCb = cv2.resize(imgC[:,1::2], (W, H), interpolation=cv2.INTER_LINEAR) - 128
    imgCr = cv2.resize(imgC[:,::2], (W, H), interpolation=cv2.INTER_LINEAR) - 128

    imgRGB = np.zeros((H, W, 3))
    imgRGB[:,:,0] = 256*imgY + 0*imgCb + 359*imgCr
    imgRGB[:,:,1] = 256*imgY - 88*imgCb - 183*imgCr
    imgRGB[:,:,2] = 256*imgY + 454*imgCb + 0*imgCr

    imgRGB = np.clip(imgRGB/256, a_min=0, a_max=255)
    return imgRGB


def simpleCFA(imgRaw):
    """
        convert bayer image to rgb image (just do a simple binning)
        :param imgRaw: a bayer image with format BGGR HxW
        :return imgCFA: an rgb image with size H/2xW/2x3
    """
    H, W = imgRaw.shape
    imgRawPad = np.zeros((H+2, W+2))
    imgRawPad[1:H+1, 1:W+1] = imgRaw
    imgRawPad[:,0] = imgRawPad[:,2]
    imgRawPad[:,W+1] = imgRawPad[:,W-1]
    imgRawPad[0,:] = imgRawPad[2,:]
    imgRawPad[H+1,:] = imgRawPad[H-1,:]
    imgCFA = np.zeros((H//2, W//2, 3))
    # G channel
    imgCFA[:,:,1] = (imgRawPad[2:H+1:2, 1:W+1:2] + imgRawPad[1:H+1:2, 2:W+1:2])/2.0
    # R channel
    kernel = np.array([[1/16, 3/16], [3/16, 9/16]], dtype=np.float32)
    imgCFA[:,:,0] = cv2.filter2D(imgRawPad[:H:2, :W:2], -1, kernel, anchor=(0, 0), borderType=cv2.BORDER_CONSTANT)
    # B channel
    kernel = np.array([[9/16, 3/16], [3/16, 1/16]], dtype=np.float32)
    imgCFA[:,:,2] = cv2.filter2D(imgRawPad[1:H+1:2, 1:W+1:2], -1, kernel, anchor=(0, 0), borderType=cv2.BORDER_CONSTANT)
    return imgCFA

def menonCFA(imgRaw):
    """
        convert bayer image to rgb image using mennon
        :param imgRaw: a bayer image with format BGGR HxW
        :return imgCFA: an rgb image with size H/2xW/2x3
    """
    from colour_demosaicing import demosaicing_CFA_Bayer_Menon2007
    imgCFA = demosaicing_CFA_Bayer_Menon2007(imgRaw, pattern='BGGR')
    return imgCFA

def simpleISP(imgCFA, bpp, DRCGain, GammaLUT, CCM, DRCSkip, debug, debug_path):
    """
        convert rgb linear image to yuv nonlinear image
        :param imgCFA: rgb linear image HxWx3 0~2**bpp-1
        :param bpp: bit per pixel of imgCFA
        :param DRCGain, GammaLUT, CCM: LUT for DRC, Gamma and CCM
        :param debug: debug level to show image after each step
        :return imgY, imgC: HxW and H/2xW
        :return imgGamma: rgb nonlinear image after gamma correction
    """
    if debug and not os.path.exists(debug_path):
        os.makedirs(debug_path)

    maxRange = 2**bpp
    H, W, N = imgCFA.shape

    if debug:
        cv2.imwrite(os.path.join(debug_path, 'imgCFA.png'), visualizeRaw(imgCFA, bpp, resize=False, color=True))

    # apply DRC
    if DRCSkip:
        imgDRC = imgCFA.copy()
    else:
        luma = np.clip(np.round(0.25*imgCFA[:,:,0] + 0.5*imgCFA[:,:,1] + 0.25*imgCFA[:,:,2]), a_min=0, a_max=maxRange-1).astype(np.uint16)
        imgDRC = np.clip(np.tile(DRCGain[luma][:,:,None], [1, 1, 3])*imgCFA, a_min=0, a_max=maxRange-1)


    if debug:
        cv2.imwrite(os.path.join(debug_path, 'imgDRC.png'), visualizeRaw(imgDRC, bpp, resize=False, color=True))

    # apply CCM
    imgDRC = np.reshape(imgDRC, (H*W, N))
    imgRGB = np.matmul(CCM, imgDRC.T)
    imgRGB = np.reshape(imgRGB.T, (H, W, N))
    imgRGB = np.clip(np.round(imgRGB), a_min=0, a_max=maxRange-1).astype(np.uint16)

    if debug:
        cv2.imwrite(os.path.join(debug_path, 'imgRGB.png'), visualizeRaw(imgRGB, bpp, resize=False, color=True))

    # apply Gamma
    imgGamma = GammaLUT[imgRGB]

    if debug:
        cv2.imwrite(os.path.join(debug_path, 'imgGamma.png'), visualizeRaw(imgGamma, 8, resize=False, color=True))

    # convert rgb to yuv
    imgyuv = rgb2yuv(imgGamma)
    imgY, imgC = yuv420(imgyuv)

    if debug:
        cv2.imwrite(os.path.join(debug_path, 'imgY.png'), visualizeRaw(imgY, 8, resize=False, color=False))
        cv2.imwrite(os.path.join(debug_path, 'imgCb.png'), visualizeRaw(imgC[:,::2], 8, resize=False, color=False))
        cv2.imwrite(os.path.join(debug_path, 'imgCr.png'), visualizeRaw(imgC[:,1::2], 8, resize=False, color=False))
    return imgY, imgC, imgGamma

def simpleInvISP(imgY, imgC, bpp, DRCInvGain, GammaInvLUT, invCCM, debug, debug_path):

    if not os.path.exists(debug_path):
        os.makedirs(debug_path)

    maxRange = 2**bpp
    H, W = imgY.shape

    if debug:
        cv2.imwrite(os.path.join(debug_path, 'imgY_before_invISP.png'), visualizeRaw(imgY, 8, resize=False, color=False))
        cv2.imwrite(os.path.join(debug_path, 'imgCb_before_invISP.png'),
                    visualizeRaw(imgC[:,::2], 8, resize=False, color=False))
        cv2.imwrite(os.path.join(debug_path, 'imgCr_before_invISP.png'),
                    visualizeRaw(imgC[:, 1::2], 8, resize=False, color=False))

    # convert yuv to rgb
    imgGamma = yuv2rgb(imgY, imgC)

    if debug:
        cv2.imwrite(os.path.join(debug_path, 'imgGamma_inv.png'), visualizeRaw(imgGamma, 8, resize=False, color=True))

    # undo Gamma
    imgGamma = np.clip(np.round(imgGamma/255*(maxRange-1)).astype(np.uint16), 0, maxRange-1)
    imgRGB = GammaInvLUT[imgGamma].astype(np.uint16)

    if debug:
        cv2.imwrite(os.path.join(debug_path, 'imgRGB_inv.png'), visualizeRaw(imgRGB, bpp, resize=False, color=True))

    # undo CMM
    imgDRC = np.reshape(imgRGB, (H*W, 3))
    imgDRC = np.matmul(invCCM, imgDRC.T)
    imgDRC = np.reshape(imgDRC.T, (H, W, 3))
    imgDRC = np.clip(np.round(imgDRC), a_min=0, a_max=maxRange-1).astype(np.uint16)

    if debug:
        cv2.imwrite(os.path.join(debug_path, 'imgDRC_inv.png'), visualizeRaw(imgDRC, bpp, resize=False, color=True))


    # undo DRC
    luma = np.clip(np.round(0.25 * imgDRC[:, :, 0] + 0.5 * imgDRC[:, :, 1] + 0.25 * imgDRC[:, :, 2]), a_min=0, a_max=maxRange - 1).astype(np.uint16)
    imgCFA = np.clip(np.tile(DRCInvGain[luma][:, :, None], [1, 1, 3]) * imgDRC, a_min=0, a_max=maxRange - 1)

    if debug:
        cv2.imwrite(os.path.join(debug_path, 'imgCFA_inv.png'), visualizeRaw(imgCFA, bpp, resize=False, color=True))

    return imgCFA




