import argparse
from collections import defaultdict
import os

import colour_demosaicing
import cv2
import numpy as np
import json
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import yaml
import matplotlib.pyplot as plt

import simple_ISP
from file_util import dump
from models import load_model, Deflare
from data import load_data
from loss import PSNR, MSSSIM, SSIM
from utils import  save_image, gaussian_pyramid, net_desc, normalize_img, torch_to_np, bggr2bayer


def simple_ISP_visual(output_folder, scene_name, data, metadata):
	print(data.min(), data.max())
	#data = torch_to_np(data)
	#vis_out = normalize_img(torch_to_np(data), metadata['blacklevel'], metadata['whitelevel'], forward=False)	
	vis_out = (data - metadata['blacklevel']) / (metadata['whitelevel'] - metadata['blacklevel']) * metadata['whitelevel']
	print(vis_out.min(), vis_out.max())
	DRCGain, _, GammaLUT, _, CCM, _ = simple_ISP.loadLUT(metadata['bpp'])
	vis_out = np.clip(np.round(vis_out), a_min=0, a_max=metadata['whitelevel'])
	vis_out_CFA = vis_out
	#vis_out_CFA = simple_ISP.menonCFA(vis_out)
	_, _, vis_out_RGB = simple_ISP.simpleISP(vis_out_CFA, metadata['bpp'], DRCGain, GammaLUT, CCM, False, 0, '')
	print(vis_out_RGB.min(), vis_out_RGB.max())
	vis_out_RGB = cv2.cvtColor(np.uint8(vis_out_RGB), cv2.COLOR_RGB2BGR)
	cv2.imwrite(os.path.join(output_folder, scene_name + '_SDK_out.png'), vis_out_RGB)

def inference_shuai_deflare(args, config, img, count):

	config_deflare = config["deflare"]

	deflare = Deflare(mask_method=config_deflare['mask_method'],
                 flare_thres=config_deflare['flare_thres'],
                 small=config_deflare['small'],
                 is_mask=config_deflare['is_mask'],
                 gray_psf_path=config_deflare['gray_psf_path'],
                 max_range=16,
                 is_Wolfgang=config_deflare['is_Wolfgang'],
                 filter_path=config_deflare['filter_path'],
                 dict_ckpt_path=config_deflare['dict_ckpt_path'],
                 device=args.device)

	img = img.squeeze(0).permute(1, 2, 0).cpu().numpy()
	bayer_img = colour_demosaicing.mosaicing_CFA_Bayer(img)
	max_range = bayer_img.max()
	bayer_img = bayer_img / max_range * 16
	
	img_deflare, mask = deflare.process(bayer_img)
	np.save(f"{args.save}/{count}_mask.npy", mask)
	
	img_deflare = img_deflare/16 * max_range
	img_deflare = colour_demosaicing.demosaicing_CFA_Bayer_Menon2007(img_deflare)
	img_deflare = torch.tensor(img_deflare.astype(np.float32)).permute(2, 0, 1).unsqueeze(0).to(args.device)

	return img_deflare

def inference(args, config, model_config, net, img, gt, count):

	if args.model_arch.startswith("MSResNet"): 
		img = [f.to(args.device) \
			for f in gaussian_pyramid(img, model_config["n_scales"])]
	else:
		img = img.to(args.device)

	if args.model_arch == "deflare":
		deflared = inference_shuai_deflare(args, config, gt, count)
	else:
		deflared = net.forward(img)

	if args.model_arch.startswith("MSResNet"):
		deflared = deflared[0]
	
	return deflared

def main(args, config):

	# TODO: Move cpt loading somewhere else
	
	if args.model_arch == "deflare":
		config["predict"]["ltm"] = False

	dataset = load_data(args, config["predict"], pred=True)
	print(len(dataset))
	test_loader = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=1)

	model_config = config[config["training"]["model"]]
	model_config["in_channels"] = dataset[0][0].shape[0] 	
	print("IN CHANNELS", model_config["in_channels"])
	net, model_config = load_model(config, args.model_arch)
	if net:
		net = nn.DataParallel(net)
		checkpoint = torch.load(args.model_cpt, map_location=args.device)
		net.load_state_dict(checkpoint["model"])
		net.to(args.device)
		print(net_desc(net))
	
	metrics = {"imgs": defaultdict(int), "mean": defaultdict(int)}

	os.makedirs(args.save, exist_ok=True)

	for count, (flare, gt, mode) in enumerate(test_loader):
		mode = mode[0]
		
		if mode == "pred":
			input_max = flare.max()
			gt_max = gt.max()
			flare = flare / flare.max()
			gt = gt / gt.max()
		
		if flare.shape[2] > config['predict']['img_size'] or flare.shape[3] > config['predict']['img_size']:
			pad_sz = 64
			patch_size = config['predict']['patch_size']
			deflared = torch.zeros(flare.size())
			'''
			for r in range(0, flare.shape[2], patch_size):
				for c in range(0, flare.shape[3], patch_size):
					img_c = F.pad(flare[:, :, r:r+patch_size, c:c+patch_size], (pad_sz, pad_sz, pad_sz, pad_sz))
					gt_c = F.pad(gt[:, :, r:r+patch_size, c:c+patch_size], (pad_sz, pad_sz, pad_sz, pad_sz)) 
					print(img_c.shape)
					deflared[:, :, r-pad_sz:r+patch_size+pad_sz, c-pad_sz:c+patch_size+pad_sz] = inference(
						args, config, model_config, net, img_c, gt_c
						)
			'''
			flare_tmp = F.pad(flare, (pad_sz, pad_sz, pad_sz, pad_sz))
			gt_tmp = F.pad(gt, (pad_sz, pad_sz, pad_sz, pad_sz))
			H1 = flare_tmp.shape[2] - 2*pad_sz; W1 = flare_tmp.shape[3] - 2*pad_sz
			deflared = torch.zeros((1, 3, H1, W1))
			ps1 = np.arange(pad_sz, H1 + pad_sz, patch_size)
			ps2 = np.arange(pad_sz, W1 + pad_sz, patch_size)

			for ii in range(len(ps1)):
				for jj in range(len(ps2)):
					x_l = ps1[ii] + np.arange(-pad_sz, min(patch_size + pad_sz, H1 + 2*pad_sz - ps1[ii]))
					y_l = ps2[jj] + np.arange(-pad_sz, min(patch_size + pad_sz, W1 + 2*pad_sz - ps2[jj]))
					Y_L, X_L = np.meshgrid(y_l, x_l)
					img_c = flare_tmp[:, :, X_L, Y_L]
					gt_c = gt_tmp[:, :, X_L, Y_L]
					
					output = inference(args, config, model_config, net, img_c, gt_c, count).detach()
					
					x_h2 = x_l[0] + np.arange(0, len(x_l) - 2*pad_sz)
					y_h2 = y_l[0] + np.arange(0, len(y_l) - 2*pad_sz)
					Y_H2, X_H2 = np.meshgrid(y_h2, x_h2)

					deflared[:, :, X_H2, Y_H2] = output[:, :, pad_sz:-pad_sz, pad_sz:-pad_sz]
			print(f"Deflared {deflared.shape}")	
		else:
			print("Running non-patched")
			deflared = inference(args, config, model_config, net, flare, gt, count).detach()

		if args.compute_metrics:

			msssim = MSSSIM().forward(deflared, gt)
			psnr = PSNR().forward(deflared, gt)
			ssim = SSIM().forward(deflared, gt)

			img_metrics = dict(
				msssim=msssim.item(),
				psnr=psnr.item(),
				ssim=ssim.item()
			)

			metrics["imgs"][count] = img_metrics
			for k in img_metrics.keys():
				metrics["mean"][k] += img_metrics[k]
		
		'''
		if config["predict"]["ltm"]:
			print("LINEAE")
			mu=1000
			print(deflared.min(), deflared.max())
			deflared =(np.exp(np.log(1+mu)*np.clip(torch_to_np(deflared), 0, 1))-1)/mu
		'''
		#if HDR:
		#	flare *=16; gt*=16; deflared*=16

		if config['predict']['BGGR']:
			print(flare.shape, flare.max())
			flare = colour_demosaicing.demosaicing_CFA_Bayer_Menon2007(bggr2bayer(torch_to_np(flare)), pattern='BGGR')
			gt = colour_demosaicing.demosaicing_CFA_Bayer_Menon2007(bggr2bayer(torch_to_np(gt)), pattern='BGGR')
			deflared = colour_demosaicing.demosaicing_CFA_Bayer_Menon2007(bggr2bayer(torch_to_np(deflared)), pattern='BGGR')
			print(flare.shape, flare.max())

		if mode == 'pred':
			cv2.imwrite(f"{args.save}/{count}_input.png", np.uint8(np.clip(flare[:, :, ::-1] * 16, 0, 1) * 255))
			cv2.imwrite(f"{args.save}/{count}_output.png", np.uint8(np.clip(deflared[:, :, ::-1] * 16, 0, 1) * 255))
			cv2.imwrite(f"{args.save}/{count}_gt.png", np.uint8(np.clip(gt[:, :, ::-1] * 16, 0, 1) * 255))

		'''
		if mode == "pred":
			print("flare", flare.min(), flare.max())
			mu =1000
			flare = flare * input_max.numpy()
			gt=(np.exp(np.log(1+mu)*np.clip(gt,0,1))-1)/mu	
			gt = gt * gt_max.numpy()
			deflared = deflared * input_max.numpy()
			print(gt.min(), gt.max())
			#simple_ISP_visual(args.save, str(count), gt, config['predict'])
			
			dump(np.clip(gt,0,config['predict']['whitelevel']),
				args.save, module_name='', posfix=f"{count}_gt",
				normalize=True, black_level=0, white_level=config['predict']['whitelevel'],
				clip=True, visualization='simpleISP', save_npy=False, save_raw=False, add_raw_blacklevel=False)
			dump(np.clip(flare,0,config['predict']['whitelevel']),
				args.save, module_name='', posfix=f"{count}_input",
				normalize=True, black_level=0, white_level=config['predict']['whitelevel'],
				clip=True, visualization='simpleISP', save_npy=False, save_raw=False, add_raw_blacklevel=True)
			
			dump(np.clip(deflared,0,config['predict']['whitelevel']),
				args.save, module_name='', posfix=f"{count}_output",
				normalize=True, black_level=0, white_level=config['predict']['whitelevel'],
				clip=True, visualization='simpleISP', save_npy=False, save_raw=False, add_raw_blacklevel=True)
		'''

		if mode == "test":
			print(flare.min(), flare.max())
			save_image(
				f"{args.save}/{count}_input.png",
				flare, depth=16, normalize=False, Gamma=2.2, inv_ltm=mode=="pred"
			)
			save_image(
				f"{args.save}/{count}_gt.png",
				gt, depth=16, normalize=False, Gamma=2.2, inv_ltm=mode=="pred"
			)
			save_image(
				f"{args.save}/{count}_output.png",
				deflared, depth=16, normalize=False, Gamma=2.2, inv_ltm=mode=="pred"
			)
			
	
	for k in metrics["mean"].keys():
		metrics["mean"][k] = metrics["mean"][k] / (count+1)
	metrics["imgs"] = dict(metrics["imgs"])
	metrics["mean"] = dict(metrics["mean"])
	
	with open(f"{args.save}/results.json", "w") as fp:
		json.dump(metrics, fp)


if __name__ == "__main__":

	parser = argparse.ArgumentParser()

	parser.add_argument("--pred_path", type=str,
						help="Path to prediction data")
	parser.add_argument("--test_path", type=str,
						help="Path to testing data")
	parser.add_argument("--save", required=True, 
						help="Save path inside of _results")
	parser.add_argument("--model_cpt", type=str, required=False,
						help="Path to model to be tested")
	parser.add_argument("--model_arch", choices=["MBCNN", "UNet", "MSResNet", "deflare", "MSResNetLBF", "MSResNetFTRB", "ResSkipC"],
						help="Model Architecture", required=True)
	parser.add_argument("--config", default="cfg.yml")
	parser.add_argument("--compute_metrics", default=True,
						help="Compute MSSIM and PSNR")
	parser.add_argument("--dataset", choices=["SpatialPSF", "LightMask", "SpatialPSFWolfgang"], default="SpatialPSF")
	parser.add_argument("--debug", action="store_true", default=False)
	args = parser.parse_args()	

	args.device = torch.device("cuda:0" if torch.cuda.is_available() \
		else "cpu")

	with open(args.config, 'r') as f:
		cfg = yaml.safe_load(f)

	main(args, cfg)