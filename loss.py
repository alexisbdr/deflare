from math import exp

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from PIL import Image
import torchvision
from torchvision import transforms
import matplotlib.pyplot as plt


class L1_Charbonnier_loss(nn.Module):
    """L1 Charbonnierloss."""
    def __init__(self):
        super(L1_Charbonnier_loss, self).__init__()
        self.eps = 1e-6

    def forward(self, X, Y):
        diff = torch.add(X, -Y)
        error = torch.sqrt(diff * diff + self.eps)
        loss = torch.sum(error) / X.size(0)
        return loss


class AdvancedSobelLoss(nn.Module):
    def __init__(self, device=torch.device('cuda'), loss=nn.L1Loss):
        super(AdvancedSobelLoss, self).__init__()

        self.device = device
        self.conv_op_x = nn.Conv2d(3, 1, 3, bias=False)
        self.conv_op_y = nn.Conv2d(3, 1, 3, bias=False)
        self.conv_op_xy = nn.Conv2d(3, 1, 3, bias=False)
        self.conv_op_yx = nn.Conv2d(3, 1, 3, bias=False)

        sobel_kernel_x = np.array([[[1, 0, -1], [2, 0, -2], [1, 0, -1]],
                                   [[1, 0, -1], [2, 0, -2], [1, 0, -1]],
                                   [[1, 0, -1], [2, 0, -2], [1, 0, -1]]], dtype='float32')
        sobel_kernel_y = np.array([[[1, 2, 1], [0, 0, 0], [-1, -2, -1]],
                                   [[1, 2, 1], [0, 0, 0], [-1, -2, -1]],
                                   [[1, 2, 1], [0, 0, 0], [-1, -2, -1]]], dtype='float32')
        sobel_kernel_xy = np.array([[[0, 1, 2], [-1, 0, 1], [-2, -1, 0]],
                                    [[0, 1, 2], [-1, 0, 1], [-2, -1, 0]],
                                    [[0, 1, 2], [-1, 0, 1], [-2, -1, 0]]], dtype='float32')
        sobel_kernel_yx = np.array([[[2, 1, 0], [1, 0, -1], [0, -1, -2]], 
                                    [[2, 1, 0], [1, 0, -1], [0, -1, -2]],
                                    [[2, 1, 0], [1, 0, -1], [0, -1, -2]]], dtype='float32')
        
        sobel_kernel_x = sobel_kernel_x.reshape((1, 3, 3, 3))
        sobel_kernel_y = sobel_kernel_y.reshape((1, 3, 3, 3))
        sobel_kernel_xy = sobel_kernel_xy.reshape((1, 3, 3, 3))
        sobel_kernel_yx = sobel_kernel_yx.reshape((1, 3, 3, 3))

        self.conv_op_x.weight.data = torch.from_numpy(sobel_kernel_x).to(device)
        self.conv_op_y.weight.data = torch.from_numpy(sobel_kernel_y).to(device)
        self.conv_op_xy.weight.data = torch.from_numpy(sobel_kernel_xy).to(device)
        self.conv_op_yx.weight.data = torch.from_numpy(sobel_kernel_yx).to(device)

        self.conv_op_x.weight.requires_grad = False
        self.conv_op_y.weight.requires_grad = False
        self.conv_op_xy.weight.requires_grad = False
        self.conv_op_yx.weight.requires_grad = False

        self.loss = loss().to(device)

    def get_sobel_output(self, x):
        
        sobel_x = self.conv_op_x(x)
        sobel_y = self.conv_op_y(x)
        sobel_xy = self.conv_op_xy(x)
        sobel_yx = self.conv_op_yx(x)
        sobel_out = torch.abs(sobel_x) + torch.abs(sobel_y) + \
            torch.abs(sobel_xy) + torch.abs(sobel_yx)

        return sobel_out

    def forward(self, img1, img2):

        sobel_1 = self.get_sobel_output(img1)
        sobel_2 = self.get_sobel_output(img2)

        loss = self.loss(sobel_1, sobel_2)

        return loss


### MSSIM AND SSIM
###
# Computing MS-SSIM (Multi-Scale Structural Similarity) scores
# The code is borrowed from the following github repository:
#   -   https://github.com/jorge-pessoa/pytorch-msssim
#   -   https://ece.uwaterloo.ca/~z70wang/research/ssim/
###
###


def gaussian(window_size, sigma):
    gauss = torch.Tensor(
        [
            exp(-((x - window_size // 2) ** 2) / float(2 * sigma ** 2))
            for x in range(window_size)
        ]
    )
    return gauss / gauss.sum()


def create_window(window_size, channel=1):
    _1D_window = gaussian(window_size, 1.5).unsqueeze(1)
    _2D_window = _1D_window.mm(_1D_window.t()).float().unsqueeze(0).unsqueeze(0)
    window = _2D_window.expand(channel, 1, window_size, window_size).contiguous()
    return window


def ssim(
    img1,
    img2,
    window_size=11,
    window=None,
    size_average=True,
    full=False,
    val_range=None,
):

    if val_range is None:
        if torch.max(img1) > 128:
            max_val = 255
        else:
            max_val = 1

        if torch.min(img1) < -0.5:
            min_val = -1
        else:
            min_val = 0
        L = max_val - min_val
    else:
        L = val_range

    padd = 0
    (_, channel, height, width) = img1.size()
    if window is None:
        real_size = min(window_size, height, width)
        window = create_window(real_size, channel=channel).to(img1.device)

    mu1 = F.conv2d(img1, window, padding=padd, groups=channel)
    mu2 = F.conv2d(img2, window, padding=padd, groups=channel)

    mu1_sq = mu1.pow(2)
    mu2_sq = mu2.pow(2)
    mu1_mu2 = mu1 * mu2

    sigma1_sq = F.conv2d(img1 * img1, window, padding=padd, groups=channel) - mu1_sq
    sigma2_sq = F.conv2d(img2 * img2, window, padding=padd, groups=channel) - mu2_sq
    sigma12 = F.conv2d(img1 * img2, window, padding=padd, groups=channel) - mu1_mu2

    C1 = (0.01 * L) ** 2
    C2 = (0.03 * L) ** 2

    v1 = 2.0 * sigma12 + C2
    v2 = sigma1_sq + sigma2_sq + C2
    cs = torch.mean(v1 / v2)

    ssim_map = ((2 * mu1_mu2 + C1) * v1) / ((mu1_sq + mu2_sq + C1) * v2)

    if size_average:
        ret = ssim_map.mean()
    else:
        ret = ssim_map.mean(1).mean(1).mean(1)

    if full:
        return ret, cs
    return ret


def msssim(
    img1, img2, window_size=11, size_average=True, val_range=None, normalize=False
):
    device = img1.device
    weights = torch.FloatTensor([0.0448, 0.2856, 0.3001, 0.2363, 0.1333]).to(device)
    levels = weights.size()[0]
    mssim = []
    mcs = []
    for _ in range(levels):
        sim, cs = ssim(
            img1,
            img2,
            window_size=window_size,
            size_average=size_average,
            full=True,
            val_range=val_range,
        )
        mssim.append(sim)
        mcs.append(cs)

        img1 = F.avg_pool2d(img1, (2, 2))
        img2 = F.avg_pool2d(img2, (2, 2))

    mssim = torch.stack(mssim)
    mcs = torch.stack(mcs)

    if normalize:
        mssim = (mssim + 1) / 2
        mcs = (mcs + 1) / 2

    pow1 = mcs ** weights
    pow2 = mssim ** weights
    output = torch.prod(pow1[:-1]) * pow2[-1]
    return output


class SSIM(torch.nn.Module):
    def __init__(self, window_size=11, size_average=True, val_range=None):
        super(SSIM, self).__init__()
        self.window_size = window_size
        self.size_average = size_average
        self.val_range = val_range

        self.channel = 1
        self.window = create_window(window_size)

    def forward(self, img1, img2):
        (_, channel, _, _) = img1.size()

        if channel == self.channel and self.window.dtype == img1.dtype:
            window = self.window
        else:
            window = (
                create_window(self.window_size, channel)
                .to(img1.device)
                .type(img1.dtype)
            )
            self.window = window
            self.channel = channel

        return ssim(
            img1,
            img2,
            window=window,
            window_size=self.window_size,
            size_average=self.size_average,
        )


class MSSSIM(torch.nn.Module):
    def __init__(self, window_size=11, size_average=True, channel=3):
        super(MSSSIM, self).__init__()
        self.window_size = window_size
        self.size_average = size_average
        self.channel = channel

    def forward(self, img1, img2):
        return msssim(
            img1, img2, window_size=self.window_size, size_average=self.size_average
        )


###
### PSNR: Peak Signal to Noise Ratio
###


class PSNR(nn.Module):
    def __init__(self, reduction: str = "mean", eps: float = 1e-8):
        self.reduction = reduction
        self.eps = eps

    def forward(self, x: torch.Tensor, y: torch.Tensor):
        """
        x & y: Predicted images & Target images
        Shape (H, W), (C, H, W) or (N, C, H, W)
        Range [0, 1] np.float32
        """

        mse = torch.mean((x - y) ** 2, dim=[1, 2, 3])
        score = -10 * torch.log10(mse + self.eps)

        return {"mean": score.mean, "sum": score.sum}[self.reduction](dim=0)


### VGG Loss

class VGGPerceptualLoss(torch.nn.Module):
    def __init__(self, device, resize=True, loss=nn.MSELoss):
        super(VGGPerceptualLoss, self).__init__()

        blocks = []
        blocks.append(torchvision.models.vgg16(pretrained=True).features[:4].eval())
        blocks.append(torchvision.models.vgg16(pretrained=True).features[4:9].eval())
        blocks.append(torchvision.models.vgg16(pretrained=True).features[9:16].eval())
        blocks.append(torchvision.models.vgg16(pretrained=True).features[16:23].eval())
        for bl in blocks:
            for p in bl:
                p.requires_grad = False

        self.blocks = torch.nn.ModuleList(blocks).to(device)
        self.transform = torch.nn.functional.interpolate
        self.mean = torch.nn.Parameter(
            torch.tensor([0.485, 0.456, 0.406], device=device).view(1, 3, 1, 1)
        )
        self.std = torch.nn.Parameter(
            torch.tensor([0.229, 0.224, 0.225], device=device).view(1, 3, 1, 1)
        )
        self.resize = resize
        self.loss = loss().to(device)

    def forward(self, input, target, feature_layers=[0, 1, 2, 3], style_layers=[]):
        if input.shape[1] != 3:
            input = input.repeat(1, 3, 1, 1)
            target = target.repeat(1, 3, 1, 1)
        input = (input - self.mean) / self.std
        target = (target - self.mean) / self.std
        if self.resize:
            input = self.transform(
                input, mode="bilinear", size=(224, 224), align_corners=False
            )
            target = self.transform(
                target, mode="bilinear", size=(224, 224), align_corners=False
            )
        loss = 0.0
        x = input
        y = target
        for i, block in enumerate(self.blocks):
            x = block(x)
            y = block(y)
            loss += self.loss(x, y)
        return loss