import argparse
import os

import numpy as np
import cv2
import torch
import torch.nn as nn
import torch.nn.functional as F
import yaml
from torch.autograd import Variable
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import DataLoader
from torchvision.utils import make_grid
from tqdm import tqdm
import matplotlib.pyplot as plt

from loss import AdvancedSobelLoss, VGGPerceptualLoss
from data import load_data 
from models import load_model, get_demosaic_net_model, predict_rgb_from_bayer_tensor
from utils import gaussian_pyramid, net_desc, bggr2bayer_tensor

torch.autograd.set_detect_anomaly(False)

def main(args, config):

	#Load network
	if args.model: 
		config["training"]["model"] = args.model
	model_config = config[config["training"]["model"]]

	dataset = load_data(args, config["training"])
	print(len(dataset))
	train_loader = DataLoader(
		dataset, batch_size=int(model_config["batch_size"]),
		num_workers=4, shuffle=True,
		pin_memory=True
	)

	model_config["in_channels"]	= dataset[0][0].shape[0]
	net, model_config = load_model(config, config["training"]["model"])
	demosaicnet = get_demosaic_net_model(
					pretrained=config["training"]["demosaicnet"],
					device=args.device
	)

	if args.multi_gpu:
		net = nn.DataParallel(net)

	net = net.to(args.device)
	print(net_desc(net))	

	#Setup L1 Loss and Sobel loss
	l1_criterion = torch.nn.L1Loss()
	vgg_criterion = VGGPerceptualLoss(args.device)
	mse_criterion = torch.nn.MSELoss()
	sobel_criterion = AdvancedSobelLoss(args.device)

	#Set up adam optimizer
	optimizer = torch.optim.Adam(
		net.parameters(), lr=float(model_config["learning_rate"])
	)

	summary = SummaryWriter(
		log_dir = args.log_dir, comment=""
	)
	count=0
	epoch_loss_plot = []
	for epoch in range(int(model_config["epochs"])):
		with tqdm(
			total=len(dataset), desc=f"Epoch {epoch+1}/{int(model_config['epochs'])}", unit="img"
		) as pbar:
			epoch_loss = 0.0
			for iteration, (flare, gt) in enumerate(train_loader):
				
				count+=1

				if config["training"]["model"].startswith("MSResNet"): 
					flare = [Variable(f).to(args.device) \
						for f in gaussian_pyramid(flare, model_config["n_scales"])]
					gt = [Variable(g).to(args.device) \
						for g in gaussian_pyramid(gt, model_config["n_scales"])]
				else:
					flare = Variable(flare).to(args.device)
					gt = Variable(gt).to(args.device)

				optimizer.zero_grad()

				deflared = net(flare)

				#sobel = float(config["lambda_sobel"]) * sobel_criterion(deflared, gt)
				if config["training"]["model"] in ["MSResNet", "MBCNN", \
					"MSResNetLBF", "MSResNetFTRB"]:
					loss = 0.0
					for i in range(len(deflared)):
						if deflared[i].shape[1] == 4:
							Bayer = torch.clamp(bggr2bayer_tensor(deflared[i], args.device), 0, 1)
							deflared[i] = predict_rgb_from_bayer_tensor(Bayer, demosaicnet, 'BGGR', args.device)
							gt[i] = gt[i][:, :, 1:-1, :]
						loss += l1_criterion(deflared[i], gt[i])
						if int(model_config['lambda_vgg']): 
							loss += float(model_config['lambda_vgg']) * vgg_criterion(deflared[i], gt[i])
						if int(model_config['lambda_vgg']):
							loss += float(model_config['lambda_sobel']) * sobel_criterion(deflared[i], gt[i])
					flare = flare[0]
					deflared = deflared[0]
					gt = gt[0]
				else:
					loss = l1_criterion(deflared, gt)
					loss += float(model_config['lambda_vgg']) * vgg_criterion(deflared, gt)
					loss += float(model_config['lambda_sobel']) * sobel_criterion(deflared, gt)
				
				loss.backward()

				torch.nn.utils.clip_grad_norm(
					net.parameters(), .5
				)
				optimizer.step()

				summary.add_scalar("Joint Loss - step", loss.item(), count)
				pbar.set_postfix(**{"loss (batch)": loss.item()})
				pbar.update(flare.shape[0])

				epoch_loss += loss.item()

			epoch_loss_plot.append(epoch_loss)				

			if (epoch + 1) % 3 == 0:
				for i in range(flare.shape[0]):
					if flare[i].shape[0] == 4:
						Bayer = torch.clamp(bggr2bayer_tensor(flare[i].unsqueeze(0), args.device), 0, 1)
						fl = predict_rgb_from_bayer_tensor(Bayer, demosaicnet, 'BGGR', args.device)
						grid_images = torch.cat((
							fl, gt[i:i+1],
							deflared[i:i+1])
						)
					else:
						grid_images = torch.cat((
							flare[i:i+1], gt[i:i+1],
							deflared[i:i+1])
						)
					grid = make_grid(
						grid_images, padding=50, normalize=True, range=(0,1)
					).unsqueeze(0)
					os.makedirs(f"checkpoints/{args.name}/images", exist_ok=True)
					grid_np = grid.squeeze(0).permute(1, 2, 0).detach().cpu().numpy() 
					cv2.imwrite(f"checkpoints/{args.name}/images/{epoch+1}E_{i+1}B.png", np.uint8(grid_np[:, :, ::-1] * 255))
					summary.add_images(f"Image Batch {i}", grid, count)

			summary.add_scalars(
				"Epoch Loss", {
					"Joint": epoch_loss / (iteration + 1),
					#"sobel": sobel_loss / (iteration + 1)
					},
				epoch
			)

			if (epoch + 1) % 15 == 0:
				os.makedirs(f"checkpoints/{args.name}", exist_ok=True)
				torch.save(
					{"model": net.state_dict(), "optimizer": optimizer.state_dict()},
					f"checkpoints/{args.name}/{epoch+1}E.pth"
				)
				plt.plot(epoch_loss_plot)
				plt.savefig(f"checkpoints/{args.name}/epoch_loss.png")

	summary.close()

if __name__ == "__main__":

	parser = argparse.ArgumentParser()

	parser.add_argument("--path", type=str, required=True,
						help="Path to training data")
	parser.add_argument("--name", help="Name of the experiment / network",
					required=True)
	parser.add_argument("--dataset", choices=["SpatialPSF", "LightMask", "SpatialPSFWolfgang"], default="SpatialPSF")
	parser.add_argument("--model", choices=["UNet", "MBCNN", "MSResNet", "MSResNetLBF", "MSResNetFTRB", "ResSkipC"], default="",
						help="Model arch to be trained, overruns model in config")
	parser.add_argument("--config", "-cfg", default="cfg.yml")
	parser.add_argument("--debug", action="store_true", default=False)
	parser.add_argument("--multi_gpu", action="store_true", help="Distribute training across multiple GPUs")
	parser.add_argument("--log_dir", default="logs/")
	args = parser.parse_args()

	args.device = torch.device("cuda" if torch.cuda.is_available() \
		else "cpu")

	with open(args.config, 'r') as f:
		cfg = yaml.safe_load(f)

	main(args, cfg)

	