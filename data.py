import csv
import json
import glob
import math
import os
import random
from abc import ABC, abstractmethod
from typing import List, Tuple

import colour_demosaicing
import cv2
import numpy as np
import scipy
import skimage.measure
import torch
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms
import matplotlib.pyplot as plt
from structures import PSF
from utils import *

####
#### DATA STRATEGY FUNCTIONS
#### see https://docs.google.com/presentation/d/14h41b9L5x-0gggHk2RXIptxTgichugiH-9qFhnMTG7A/edit?usp=sharing
#### Follow the OOP Strategy Pattern, see example: https://refactoring.guru/design-patterns/strategy/python/example


class DataCreator(ABC):
    def __init__(self, config):
        """Base Class for data creation, extended by different data schemas"""
        self.config = config

        if config["psf"].lower() == "xm":
            k1_path = config["xm_k1_path"]
            k2_path = config["xm_k2_path"]
        elif config["psf"].lower() == "zte":
            k1_path = config["zte_k1_path"]
            k2_path = config["zte_k2_path"]
        else:
            raise Exception("Incorrect PSF selection in PSF, must be ZTE or XM")

        self.psf_obj = PSF(k1_path=k1_path, k2_path=k2_path)
        self.psf_obj.load()
        self.count = 0

        super(DataCreator).__init__()

    @classmethod
    def preprocess_images(
        cls,
        img: np.ndarray,
        img_size: int = None,
        crop: str = "",
        clip_val: float = 0,
        ltm: bool = False,
        crop_params: list = []
    ) -> torch.tensor:
        """
        img: Hazy HDR numpy array (np.float32) image in range [0 - max]
        returns max normalized torch tensor in [B, C, H, W]
        """
        img = img.astype(np.float32)
        # Normalization
        if clip_val:
            img = np.clip(img, 0, clip_val)
            img = img / clip_val
        if ltm:
            mu = 1000
            img = np.log(1 + mu * img) / np.log(1 + mu)
        torch_img = torch.from_numpy(img).float()
        torch_img = torch_img.permute(2, 0, 1)

        if img_size and img_size < torch_img.shape[1] and crop:
            if crop == "Five":
                torch_img = transforms.FiveCrop(int(img_size)).forward(torch_img)
            elif crop == "Random":
                if not crop_params:
                    crop_params = transforms.RandomCrop(int(img_size)).get_params(
                        transforms.functional.to_pil_image(torch_img), (img_size, img_size))
                torch_img = functional_crop(torch_img, crop_params, "first") 
            elif crop == "Center":
                torch_img = center_crop(
                    torch_img, img_size, img_size, "first"
                )
        #elif img_size:
        #    pass
        #    torch_img = transforms.Resize(
        #        int(img_size), interpolation=Image.NEAREST
        #    ).forward(torch_img)

        return torch_img, crop_params

    
    def augment_K2(self, augment: bool = True) -> np.ndarray:
        """Augments the K2 Matrix by applying randomly scaled Torchvision Transforms"""
        k2 = self.psf_obj.k2(f="torch", normalize=False)  # C,H,W
        if augment:
            composed = transforms.Compose(
                [
                    transforms.ColorJitter(brightness=0.1),
                    transforms.RandomPerspective(distortion_scale=0.1, p=0.75),
                ]
            )
            k2 = composed(k2)

        k2 = k2.permute(1, 2, 0).numpy()

        return k2

    @abstractmethod
    def create_data(self, p: str):
        """Method to load data from dataset containing point source intensity masks
        HDRiHaven dehaze dataset created by Alexisbaudron
        """
        self.intensity_mask = np.load(f"{p}/light_mask.npy")
        self.gt_img = np.load(f"{p}/gt.npy")

        self.k1 = self.psf_obj.k1(f="np", normalize=False)
        self.k2 = self.augment_K2()


class DataSchemeFlareSanityCheck(DataCreator):
    def center_light_source(self, img: np.ndarray):
        """Centers the light source image based on center of mass
        """
        #1. Threshold the image
        thresh = 1e-1
        psf_thresh = np.where(img > thresh, 1.0, 0.0).astype(np.uint8) #Creates the "label image"

        #Calc regions with skimage
        region = skimage.measure.regionprops(psf_thresh, img)[0]
        
        center_x = img.shape[0] // 2
        center_y = img.shape[1] // 2

        x_trans = int(center_x - region.centroid[0])
        y_trans = int(center_y - region.centroid[1])

        if x_trans or y_trans:
            
            if len(img.shape) == 3:
                img = scipy.ndimage.shift(img, (x_trans, y_trans, 0), mode='wrap')
            else:
                img = scipy.ndimage.shift(img, (x_trans, y_trans))

        else: 
            print("PSF is at the CENTER")
        
        return img
    
    def create_data(self, p: str):
        """Generates an HDR flare on a black bakground 
        """
        super().create_data(p)

        '''
        self.intensity_mask = self.center_light_source(self.intensity_mask)
        sz = int(self.config["img_size"])
        self.intensity_mask = cv2.resize(self.intensity_mask, (sz//2, sz//2))
        print(f"Intensity mask {self.intensity_mask.shape}")
        self.k2 = center_crop(self.k2, width = sz*2, height = sz*2)
        print(f"K2 {self.k2.shape}")
        '''

        flare = cv2.filter2D(self.intensity_mask, -1, self.k1)
        gt_img = np.zeros_like(flare)

        torch_flare, cp = self.preprocess_images(
            flare, int(self.config["img_size"]), 
            self.config["crop"],
            float(self.config["img_clip_val"])
        )
        torch_gt, cp = self.preprocess_images(
            gt_img, int(self.config["img_size"]),
            self.config["crop"], 
            float(self.config["img_clip_val"])
        )

        return torch_flare, torch_gt


class DataSchemeFlareK2(DataCreator):
    def create_data(self, p: str):
        super().create_data(p)

        psf = self.k1 + self.k2
        psf = awb_grayworld(psf.astype(np.float32)) 
        #psf = rgb_2_gray3(psf.astype(np.float32))
        flared = cv2.filter2D(self.gt_img, -1, psf)
        torch_conved, cp = self.preprocess_images(
            flared, int(self.config["img_size"]),
            self.config["crop"],
            float(self.config["img_clip_val"])
        )
        torch_gt, cp = self.preprocess_images(
            self.gt_img, int(self.config["img_size"]),
            self.config["crop"],
            float(self.config["img_clip_val"]),
            cp
        )

        return torch_conved, torch_gt


data_scheme_map = {
    "sanity": DataSchemeFlareSanityCheck,
    "flare_k2": DataSchemeFlareK2
}

###
### Pytorch Dataset Wrapper that handles paths and loading
###


class HDRiHavenMaskDataset(Dataset):
    def __init__(self, name: str, path: str, config, debug: bool):

        self.name = name
        self.config = config
        self.debug = debug

        self._strat = data_scheme_map.get(config["data_scheme"], None)
        if self._strat is None:
            raise ValueError(f"Incorrect Data Scheme {config['data_scheme']}")
        self._strategy = self._strat(config)

        self.paths = []
        for img_folder in os.listdir(path):
            img_folder_path = os.path.join(f"{path}/{img_folder}")
            if os.path.isdir(img_folder_path):
                self.paths.append(img_folder_path)

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, idx: int):

        p = self.paths[idx]

        flare, gt = self._strategy.create_data(p)

        if self.debug:
            os.makedirs("data/debug", exist_ok=True)
            save_image(
                f"data/debug/{idx}_flare.png",
                flare, depth=16, normalize=False
            )
            save_image(
                f"data/debug/{idx}_gt.png",
                gt, depth=16, normalize=False
            )            
        return flare, gt

idx_lookup = {
    0: (0, 0), 1: (0, 1), 2: (0, 2), 3:(0, 3),
    4: (1, 0), 5: (1, 1), 6: (1, 2), 7:(1, 3),
    8: (2, 0), 9: (2, 1), 10: (2, 2), 11:(2, 3)
}

class HDRiHavenSpatialPSFDataset(Dataset):

    def __init__(self, path: str, config, debug: bool):

        self.config = config
        self.debug = debug

        self.paths = []
        if config['SpatialPSF_json_image_filter']:
            with open(config['SpatialPSF_json_image_filter'], 'r') as f:
                dataset_dict = json.load(f)
                flare_imgs = dataset_dict['flare']['train']
            for img_name in flare_imgs:
                #img_name_npy = img_name.replace("jpg", "npy")
                img_name_npy = img_name.replace(".npy", "_flare.npy")
                img_path = os.path.join(path, img_name_npy)
                if os.path.isfile(img_path):
                    self.paths.append(img_path)
        elif config['SpatialPSF_list']:
            self.allowed_scenes = config["SpatialPSF_list"].split()
            for scene in self.allowed_scenes:
                path_to_scene = os.path.join(path, scene)
                imgs = glob.glob(path_to_scene)
                for img_f_path in imgs:
                    if img_f_path.endswith((".npy", ".jpg")):
                        self.paths.append(img_f_path)
        else:
            for img_f in os.listdir(path):
                img_f_path = os.path.join(path, img_f)
                if img_f_path.endswith((".npy", ".jpg")):
                    self.paths.append(img_f_path)

        if config["train_size"] and config["train_size"] < len(self.paths):
            self.paths = self.paths[:config["train_size"]]
        
        self.gt_paths = []
        self.crop_nums = []
        for img_flare in self.paths:
            gt_img_path = img_flare.replace("sim", "gt").replace("rgb", "")
            gt_img_path = gt_img_path.replace("_flare", "")
            #gt_img_path = gt_img_path.replace(f"{self.config['psf']}/", "")    
            #self.crop_nums.append(int(gt_img_path.split("_")[-1].strip(".npy")))
            #gt_img_path = '_'.join(gt_img_path.split("_")[:-1]) + ".npy"
            if gt_img_path.endswith((".npy", ".jpg")):
                self.gt_paths.append(gt_img_path)

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, idx: int):
        
        if self.paths[idx].endswith(".jpg"):
            flare_img = cv2.resize(cv2.imread(self.paths[idx])[:, :, ::-1].astype(np.float32) / 255, (1024, 1024))
            gt_img = cv2.resize(cv2.imread(self.gt_paths[idx])[:, :, ::-1].astype(np.float32) / 255, (1024, 1024))
            
            torch_img = torch.from_numpy(flare_img).permute(2, 0, 1)
            torch_gt = torch.from_numpy(gt_img).permute(2, 0, 1)

            return torch_img, torch_gt

        flare_img_tmp = np.load(self.paths[idx])
        gt_img = np.load(self.gt_paths[idx])

        sz = flare_img_tmp.shape[0]
        r, c = idx_lookup[self.crop_nums[idx]]
        gt_img = gt_img[r*sz: r*sz + sz, c*sz: c*sz + sz]
        
        flare_img = np.zeros((sz, sz, 3))
        flare_img[:, :flare_img_tmp.shape[1], :] = flare_img_tmp

        torch_flare, cp = DataCreator.preprocess_images(
            flare_img, int(self.config["img_size"]),
            self.config["crop"],
            float(self.config["img_clip_val"]),
            self.config["ltm"]
        )
        torch_gt, cp = DataCreator.preprocess_images(
            gt_img, int(self.config["img_size"]),
            self.config["crop"],
            float(self.config["img_clip_val"]),
            self.config["ltm"],
            cp
        )
        
        if self.debug:
            path = "data/debugltm"
            os.makedirs(path, exist_ok=True)
            save_image(
                f"{path}/{idx}_flare.png",
                torch_flare, depth=16, normalize=False, Gamma=2.2
            )
            save_image(
                f"{path}/{idx}_gt.png",
                torch_gt, depth=16, normalize=False, Gamma=2.2
            )    
        
        return torch_flare, torch_gt


class HDRiHavenSpatialPSFDatasetPred(Dataset):

    def __init__(self, pred_path: str, test_path: str, config, debug: bool):

        self.config = config
        self.debug = debug

        self.paths = [] 
        for img_f in os.listdir(pred_path):
            img_f_path = os.path.join(pred_path, img_f)
            if img_f_path.endswith(".npy"):
                self.paths.append(img_f_path)
        
        if test_path:
            test_paths = []
            if config['SpatialPSF_json_image_filter']: 
                with open(config['SpatialPSF_json_image_filter'], 'r') as f:
                    dataset_dict = json.load(f)
                    flare_imgs = dataset_dict['test']
                for img_name in flare_imgs:
                    img_name_npy = img_name.replace("jpg", "npy")
                    img_path = os.path.join(test_path, img_name_npy)
                    if os.path.isfile(img_path):
                        test_paths.append(img_path)
            else:
                for img_f in os.listdir(test_path):
                    img_f_path = os.path.join(test_path, img_f)
                    if img_f_path.endswith((".npy", ".jpg")):
                        test_paths.append(img_f_path)

            if config["test_size"] and config["test_size"] < len(test_paths):
                test_paths = test_paths[:config["test_size"]]
            
            self.paths += test_paths
        
        self.gt_paths = []
        self.crop_nums = []
        for img_flare in self.paths:
            gt_img_path = img_flare.replace("sim", "gt")
            #gt_img_path = gt_img_path.replace(f"{self.config['psf']}/", "")    
            '''
            try:
                self.crop_nums.append(int(gt_img_path.split("_")[-1].strip(".npy")))
                gt_img_path = '_'.join(gt_img_path.split("_")[:-1]) + ".npy"
                self.gt_paths.append(gt_img_path)
            except ValueError:
                self.crop_nums.append(-1)
            '''
            self.crop_nums.append(-1)
            self.gt_paths.append(gt_img_path)
        
        print(self.paths)
    
    def __len__(self):
        return len(self.paths)

    def __getitem__(self, idx: int):

        print(self.paths[idx])

        if self.paths[idx].endswith(".jpg"):
            flare_img = cv2.resize(cv2.imread(self.paths[idx])[:, :, ::-1].astype(np.float32) / 255, (1024, 1024))
            gt_img = cv2.resize(cv2.imread(self.gt_paths[idx])[:, :, ::-1].astype(np.float32) / 255, (1024, 1024))
            
            torch_img = torch.from_numpy(flare_img).permute(2, 0, 1)
            torch_gt = torch.from_numpy(gt_img).permute(2, 0, 1)

            return torch_img, torch_gt, False

        flare_img = np.load(self.paths[idx])
        gt_img = np.load(self.gt_paths[idx])

        if len(flare_img.shape) == 2:
            #flare_img = center_crop(flare_img,
            #    int(self.config["img_size"]), int(self.config["img_size"]), "last")
            flare_img = flare_img / flare_img.max()
            flare_img = colour_demosaicing.demosaicing_CFA_Bayer_Menon2007(flare_img, pattern="BGGR").astype(np.float32)
            gt_img = flare_img * 16
            flare_img = flare_img * 16


            torch_flare, cp = DataCreator.preprocess_images(
                flare_img, int(self.config["img_size"]),
                self.config["crop"]
            )
            torch_gt, cp = DataCreator.preprocess_images(
                gt_img, int(self.config["img_size"]),
                self.config["crop"], cp
            )

            return torch_flare, torch_gt, True    

        if self.crop_nums[idx] >= 0:
            sz = flare_img.shape[0]
            r, c = idx_lookup[self.crop_nums[idx]]
            gt_img = gt_img[r*sz: r*sz + sz, c*sz: c*sz + sz]
            
            flare_img_tmp = flare_img.copy()
            flare_img = np.zeros((sz, sz, 3))
            flare_img[:, :flare_img_tmp.shape[1], :] = flare_img_tmp

        torch_flare, cp = DataCreator.preprocess_images(
            flare_img, int(self.config["img_size"]),
            self.config["crop"],
            float(self.config["img_clip_val"]),
            self.config["ltm"]
        )
        torch_gt, cp = DataCreator.preprocess_images(
            gt_img, int(self.config["img_size"]),
            self.config["crop"],
            float(self.config["img_clip_val"]),
            self.config["ltm"],
            cp
        )
        
        if self.debug:
            path = "data/debugltm"
            os.makedirs(path, exist_ok=True)
            save_image(
                f"{path}/{idx}_flare.png",
                torch_flare, depth=16, normalize=False, Gamma=0
            )
            save_image(
                f"{path}/{idx}_gt.png",
                torch_gt, depth=16, normalize=False, Gamma=0
            )    
        
        return torch_flare, torch_gt, False

class HDRiHavenSpatialPSFDatasetPredWolfgang(Dataset):

    def __init__(self, pred_path: str, test_path: str, config, debug: bool):

        self.config = config
        self.debug = debug

        self.paths = {}
        count = 0 
        if pred_path:
            for img_f in os.listdir(pred_path):
                img_f_path = os.path.join(pred_path, img_f)
                if img_f_path.endswith(".npy"):
                    self.paths[count] = [img_f_path, "pred"]
                    count += 1

        if test_path:
            test_paths = []
            if config['SpatialPSF_json_image_filter']: 
                with open(config['SpatialPSF_json_image_filter'], 'r') as f:
                    dataset_dict = json.load(f)
                    flare_imgs = dataset_dict['test']
                for img_name in flare_imgs:
                    img_name_npy = img_name.replace(".npy", f"_{config['img_posfix']}.npy")
                    #img_name_npy = img_name.replace("jpg", "npy")
                    img_path = os.path.join(test_path, img_name_npy)
                    if os.path.isfile(img_path):
                        test_paths.append(img_path)
            else:
                for img_f in os.listdir(test_path):
                    img_f_path = os.path.join(test_path, img_f)
                    if img_f_path.endswith((".npy", ".jpg")):
                        test_paths.append(img_f_path)

            if config["test_size"] and config["test_size"] < len(test_paths):
                test_paths = test_paths[:config["test_size"]]
            
            for tp in test_paths:
                self.paths[count] = [tp, "test"]                
                count += 1

        self.gt_paths = []
        self.crop_nums = []
        for img_flare, mode in self.paths.values():
            if mode == "pred":
                self.gt_paths.append(img_flare)
                self.crop_nums.append(-1)
                continue
            if config['img_posfix'] in img_flare:
                gt_img_path = img_flare.replace("sim", "gt").replace("rgb", "").replace("bggr", "")
                gt_img_path = gt_img_path.replace(f"_{config['img_posfix']}", "")
                self.crop_nums.append(-1)
                self.gt_paths.append(gt_img_path)
            else:
                gt_img_path = img_flare.replace("sim", "gt")
                try:
                    self.crop_nums.append(int(gt_img_path.split("_")[-1].strip(".npy")))
                    gt_img_path = '_'.join(gt_img_path.split("_")[:-1]) + ".npy"
                    self.gt_paths.append(gt_img_path)
                except ValueError:
                    self.crop_nums.append(-1)
                    self.gt_paths.append(gt_img_path)

        print(self.paths)
        print(self.gt_paths)

        self.wolf_filter, self.wolf_filter_bggr = self.load_wolf_psf()
    
    def __len__(self):
        return len(self.paths.keys())
    
    def load_wolf_psf(self):
        k1 = np.load(self.config['PSF_path_K1'])[0:-1, 0:-1, :]
        k2 = np.load(self.config['PSF_path_K2'])[0:-1, 0:-1, :]
        
        #Scale k1
        k1 = k1 * float(self.config['wolfgang_K1_ratio'])

        k1 = color_balance(k1)
        k2 = color_balance(k2)

        PSF = k1 + k2

        #Channel Normalize K1 & K2
        for c in range(3):
            k1[:, :, c] = k1[:, :, c] / PSF[:, :, c].sum()
            k2[:, :, c] = k2[:, :, c] / PSF[:, :, c].sum()

        k1=np.pad(k1,([64,64],[64,64],[0,0]),'constant')
        k2=np.pad(k2,([64,64],[64,64],[0,0]),'constant') 
        
        #Normalize K1 and K2
        wolf = np.zeros_like(k1, dtype=np.float32)
        for c in range(3):

            #Calculate M1 and M2 from wolfgang formula
            M1 = k1[:, :, c] - k2[:, :, c]
            k1_2 = convolve2d(k1[:, :, c], k1[:, :, c])
            k2_2 = convolve2d(k2[:, :, c], k2[:, :, c])
            M2 = k1_2 + k2_2

            M3 = convolve2d(M2, M1)
            wolf[:, :, c] = M3 / M3.sum()

        wolf = center_psf_mass(wolf)
        wolf_bayer = colour_demosaicing.mosaicing_CFA_Bayer(wolf, pattern='BGGR')
        wolf_bggr = bayer2bggr(wolf_bayer)
        return wolf, wolf_bggr

    def wolf_conv(self, img, pad_size: int = 64):
        '''Handles Convolution & padding with the wolfgang kernel
        Only handles square images for now
        '''
        
        torch = False
        if not isinstance(img, np.ndarray):
            torch = True
            img = torch_to_np(img)

        wolf_filter = self.wolf_filter if img.shape[-1] == 3 else self.wolf_filter_bggr

        orig_H, orig_W = img.shape[0:2]
        Hf,Wf= wolf_filter.shape[0], wolf_filter.shape[1]
        Hb,Wb= img.shape[0], img.shape[1]
        
        if Hf<Hb and Wf<Wb: #filter size < image size, pad
            pad_h = (Hb - Hf) // 2
            pad_w = (Wb - Wf) // 2
            filter_resize=np.pad(wolf_filter,([pad_h,pad_h],[pad_w,pad_w],[0,0]),'constant')

        elif Hf<Hb and Wf>=Wb:
            pad_h = (Hb - Hf) // 2
            edge_w = (Wf - Wb) // 2
            filter_resize = np.pad(wolf_filter, ([pad_h, pad_h], [0, 0], [0, 0]), 'constant')
            filter_resize = filter_resize[:, edge_w:-edge_w, :]

        elif Hf>=Hb and Wf<Wb:
            pad_w = (Wb - Wf) // 2
            edge_h = (Hf - Hb) // 2
            filter_resize = np.pad(wolf_filter, ([0, 0], [pad_w,pad_w], [0, 0]), 'constant')
            filter_resize = filter_resize[edge_h:-edge_h, :, :]

        else: #filter size > image size, crop
            edge_h = (Hf - Hb) // 2
            edge_w = (Wf - Wb) // 2
            filter_resize=wolf_filter[edge_h:(Hf-edge_h),edge_w:(Wf-edge_w),:]
        
        img = np.pad(img,([pad_size, pad_size], [pad_size, pad_size], [0, 0]),'symmetric') 
        filter_resize = np.pad(filter_resize,([pad_size, pad_size], [pad_size, pad_size], [0, 0]),'constant') 
        
        out = np.zeros_like(img, np.float64)
        for c in range(out.shape[-1]):
            wolf = filter_resize[:, :, c] / filter_resize[:, :, c].sum()
            out[:, :, c] = convolve2d(wolf, img[:, :, c])

        #Scale back to 0-1 
        #out = (out - out.min()) / (out.max() - out.min())
        out = center_crop(out, orig_W, orig_H, "last")
        
        out = np.clip(out, 0, 16)
        if torch:
            out = np_to_torch(out)
        
        return out

    def debug_func(self, idx, torch_flare, torch_gt):
        
        if self.debug:
            path = "data/debugwolf"
            os.makedirs(path, exist_ok=True)
            save_image(
                f"{path}/{idx}_flare.png",
                torch_flare * 16, depth=8, normalize=False, Gamma=2.2
            )
            save_image(
                f"{path}/{idx}_gt.png",
                torch_gt * 16, depth=8, normalize=False, Gamma=2.2
            )    
    
    def __getitem__(self, idx: int):

        print(self.paths[idx])
        pth, mode = self.paths[idx]
        
        if pth.endswith(".jpg"):
            flare_img = cv2.resize(cv2.imread(pth)[:, :, ::-1].astype(np.float32) / 255, (1024, 1024))
            gt_img = cv2.resize(cv2.imread(self.gt_paths[idx])[:, :, ::-1].astype(np.float32) / 255, (1024, 1024))

            flare_img = self.wolf_conv(flare_img)            
            torch_img = torch.from_numpy(flare_img).permute(2, 0, 1)
            torch_gt = torch.from_numpy(gt_img).permute(2, 0, 1)

            return torch_img, torch_gt, mode

        flare_img = np.load(pth)    
        print(f"Input {mode} {flare_img.shape}, {flare_img.min()}, {flare_img.max()}")
        gt_img = np.load(self.gt_paths[idx])

        if len(flare_img.shape) == 2:
            #flare_img = center_crop(flare_img,f
            #    int(self.config["img_size"]), int(self.config["img_size"]), "last")
            input_max = flare_img.max()
            flare_img = flare_img / flare_img.max()
            #flare_img = (flare_img - self.config['blacklevel'])
            if self.config['BGGR']:
                flare_img = bayer2bggr(flare_img)
            else:
                flare_img = colour_demosaicing.demosaicing_CFA_Bayer_Menon2007(flare_img, pattern="BGGR").astype(np.float64)
            gt_img = flare_img * 16
            flare_img = flare_img * 16
            #flare_img = np.clip(flare_img, 0, 1)
            #flare_img = self.wolf_conv(flare_img)    
            torch_flare, cp = DataCreator.preprocess_images(
                flare_img, int(self.config["img_size"]),
                self.config["crop"],
                float(self.config["img_clip_val"]),
                self.config["ltm"]
            )

            torch_gt, cp = DataCreator.preprocess_images(
                gt_img, int(self.config["img_size"]),
                self.config["crop"], 
                float(self.config["img_clip_val"]),
                self.config["ltm"],
                cp
            )
            self.debug_func(idx, torch_flare, torch_gt)
            #print((torch_gt * input_max).min(), (torch_gt * input_max).max())
            return torch_flare, torch_gt, mode   

        if self.crop_nums[idx] >= 0:
            sz = flare_img.shape[0]
            r, c = idx_lookup[self.crop_nums[idx]]
            gt_img = gt_img[r*sz: r*sz + sz, c*sz: c*sz + sz]
            
            flare_img_tmp = flare_img.copy()
            flare_img = np.zeros((sz, sz, 3))
            flare_img[:, :flare_img_tmp.shape[1], :] = flare_img_tmp

        torch_flare, cp = DataCreator.preprocess_images(
            flare_img, int(self.config["img_size"]),
            self.config["crop"],
            float(self.config["img_clip_val"]),
            self.config["ltm"]
        )
        torch_gt, cp = DataCreator.preprocess_images(
            gt_img, int(self.config["img_size"]),
            self.config["crop"],
            float(self.config["img_clip_val"]),
            cp
        )

        if mode == "pred":
            torch_flare = self.wolf_conv(torch_flare)

        self.debug_func(idx, torch_flare, torch_gt)        
        
        return torch_flare, torch_gt, mode

class HDRiHavenSpatialPSFDatasetWolfgang(Dataset):

    def __init__(self, path: str, config, debug: bool):

        self.config = config
        self.debug = debug

        self.paths = []
        if config['SpatialPSF_json_image_filter']:
            with open(config['SpatialPSF_json_image_filter'], 'r') as f:
                dataset_dict = json.load(f)
                flare_imgs = dataset_dict['train']
                random.shuffle(flare_imgs)
            for img_name in flare_imgs:
                img_name = img_name.replace(".npy", f"_{config['img_posfix']}.npy")
                img_path = os.path.join(path, img_name)
                if os.path.isfile(img_path):
                    self.paths.append(img_path)
        elif config['SpatialPSF_list']:
            self.allowed_scenes = config["SpatialPSF_list"].split()
            for scene in self.allowed_scenes:
                path_to_scene = os.path.join(path, scene)
                imgs = glob.glob(path_to_scene)
                for img_f_path in imgs:
                    if img_f_path.endswith((".npy", ".jpg")):
                        self.paths.append(img_f_path)
        else:
            for img_f in os.listdir(path):
                img_f_path = os.path.join(path, img_f)
                if img_f_path.endswith((".npy", ".jpg")):
                    self.paths.append(img_f_path)

        if config["train_size"] and config["train_size"] < len(self.paths):
            self.paths = self.paths[:config["train_size"]]
        
        self.gt_paths = []
        self.crop_nums = []
        for img_flare in self.paths:
            gt_img_path = img_flare.replace("sim", "gt").replace("rgb", "").replace("bggr", "")
            gt_img_path = gt_img_path.replace(f"_{config['img_posfix']}", "")
            #gt_img_path = img_flare.replace('wolfgang', 'gt_wolfgang')
            #gt_img_path = gt_img_path.replace(f"{self.config['psf']}/", "")    
            #gt_img_path = '_'.join(gt_img_path.split("_")[:-1]) + ".npy"
            if gt_img_path.endswith((".npy", ".jpg")):
                self.gt_paths.append(gt_img_path)

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, idx: int):
        
        flare_img = np.load(self.paths[idx])
        gt_img = np.load(self.gt_paths[idx])

        torch_flare, cp = DataCreator.preprocess_images(
            flare_img, int(self.config["img_size"]),
            self.config["crop"],
            float(self.config["img_clip_val"]),
            self.config["ltm"]
        )
        torch_gt, cp = DataCreator.preprocess_images(
            gt_img, int(self.config["img_size"]),
            self.config["crop"],
            float(self.config["img_clip_val"]),
            self.config["ltm"],
            cp
        )
        
        if self.debug:
            path = "data/debugltm"
            os.makedirs(path, exist_ok=True)
            save_image(
                f"{path}/{idx}_flare.png",
                torch_flare, depth=16, normalize=False, Gamma=2.2
            )
            save_image(
                f"{path}/{idx}_gt.png",
                torch_gt, depth=16, normalize=False, Gamma=2.2
            )    
        
        return torch_flare, torch_gt

    

def load_data(args, config, pred: bool = False) -> Dataset:
    """The dataloader factory function
    """
    if pred and args.dataset != "SpatialPSFWolfgang":
        return HDRiHavenSpatialPSFDatasetPred(args.pred_path, args.test_path, config, args.debug)
    elif pred and args.dataset == "SpatialPSFWolfgang":
        return HDRiHavenSpatialPSFDatasetPredWolfgang(args.pred_path, args.test_path, config, args.debug)
    if args.dataset == "SpatialPSF":
        dataset = HDRiHavenSpatialPSFDataset(args.path, config, args.debug)
    elif args.dataset == "SpatialPSFWolfgang":
        dataset = HDRiHavenSpatialPSFDatasetWolfgang(args.path, config, args.debug)
    elif args.dataset == "LightMask":
        dataset = HDRiHavenMaskDataset(args.path, config, args.debug)
    return dataset