"""
Data structures
"""
from dataclasses import dataclass

import cv2
import numpy as np
import torch


@dataclass
class PSF:

    k2_path: str = ""
    k1_path: str = ""

    _k2: torch.tensor = None
    _k1: torch.tensor = None
    psf: torch.tensor = None

    name: str = ""

    def k1(self, f: str = "torch", normalize=True):
        return self.get_k(self._k1, f, normalize)

    def k2(self, f: str = "torch", normalize=True):
        return self.get_k(self._k2, f, normalize)

    def get_k(self, obj: np.ndarray, f, norm):
        """Implements a getter method for kernels
        f: "torch" or "np"
        norm: applies channel-wise nuclear normalization to k using the psf
        """
        if norm:
            for c in range(3):
                obj[:, :, c] = obj[:, :, c] / self.psf[:, :, c].sum()

        if f == "torch":
            return torch.from_numpy(obj).permute(2, 0, 1).float()
        elif f == "np":
            return obj
        else:
            raise ValueError("f must be one of torch or np")

    def default_kernel(self, obj: np.ndarray) -> np.ndarray:
        if obj is None:
            print()
            obj = np.zeros(self.shape)
        return obj

    def load(self):
        """Loads k1 and k2 into torch tensors
        If one of the paths isn't provided - load default tensor of zeros
        """
        if self.k1_path:
            self._k1 = np.load(self.k1_path)[:-1, :-1, :]
            # self.k1 = torch.from_numpy(np.load(self.k1_path))[:-1, :-1, :].permute(2, 0, 1).float()
        if self.k2_path:
            self._k2 = np.load(self.k2_path)[:-1, :-1, :]
            # self.k2 = torch.from_numpy(np.load(self.k2_path))[:-1, :-1, :].permute(2, 0, 1).float()
        if self.k2 is None and self.k1 is None:
            raise Exception("PSF requires at least one path for either k1 or k2")

        self.shape = self._k1.shape if self._k1 is not None else self._k2.shape
        self._k1 = self.default_kernel(self._k1)
        self._k2 = self.default_kernel(self._k2)

        self.psf = self._k1 + self._k2
        self.psf_norm = self.psf.copy()
        # Channel-wise normalization
        for c in range(3):
            self.psf_norm[:, :, c] = self.psf[:, :, c] / self.psf[:, :, c].sum()

    def save(self, name: str = ""):
        k1_path = self.k1_path.strip(".npy") + name + ".npy"
        k2_path = self.k2_path.strip(".npy") + name + ".npy"
        print(k1_path, k2_path)
        K1 = self.k1("np", normalize=True)
        K2 = self.k2("np", normalize=True)

        np.save(k1_path, K1)
        np.save(k2_path, K2)

    def A(self, img: np.ndarray) -> np.ndarray:
        return cv2.filter2D(img, -1, self.psf_norm)


# TODO: Generalized image interface for loading images
# class Image:
