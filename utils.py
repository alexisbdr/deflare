import cv2
import numpy as np
import torch
import os
from collections import OrderedDict

import colour_demosaicing
import cv2
import numpy as np
import scipy
import skimage.measure
import torch
import torch.nn.functional as F

def normalize_img(input, blacklevel, whitelevel, forward=True):
    if forward:
        return (input - blacklevel) / (whitelevel - blacklevel)
    else:
        return input * (whitelevel - blacklevel) + blacklevel

def np_to_torch(x: np.ndarray) -> torch.tensor:
    return torch.from_numpy(x).permute(2, 0, 1).float()


def torch_to_np(x: torch.tensor) -> np.ndarray:
    if len(x.size()) == 4:
        x = x.squeeze(0)
    return x.permute(1, 2, 0).detach().cpu().numpy().astype(np.float32)

def rgb_2_gray3(img: np.ndarray):
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    for c in range(3):
        img[:, :, c] = gray

    return img

def convolve2d(a,K):
    '''
    convolve the image a with kernel K
    '''
    Fa=np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(a)))
    FK=np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(K)))

    return np.real(np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(Fa*FK))))

# Taken from https://bitbucket.org/sensebrain/dehazenet/src/master/utils.py
def bggr2bayer(bggr):
    H = np.size(bggr, 0)
    W = np.size(bggr, 1)
    H2 = H * 2
    W2 = W * 2
    Bayer = np.zeros((H2, W2))

    Bayer[0:H2:2, 0:W2:2] = bggr[:, :, 0]  # B
    Bayer[0:H2:2, 1:W2:2] = bggr[:, :, 1]  # G1
    Bayer[1:H2:2, 0:W2:2] = bggr[:, :, 2]  # G2
    Bayer[1:H2:2, 1:W2:2] = bggr[:, :, 3]  # R

    return Bayer

def bggr2bayer_tensor (bggr,device): #Bayer is BGGR
    batch_size=bggr.shape[0]
    H=bggr.shape[2]
    W=bggr.shape[3]
    H2=H*2
    W2=W*2
    Bayer = torch.zeros([batch_size, 1, H2, W2], dtype=torch.float32, device=device)

    Bayer[:, 0, 0:H2:2, 0:W2:2] = bggr[:, 0, :, :]  #B
    Bayer[:, 0, 0:H2:2, 1:W2:2] = bggr[:, 1, :, :]  # G1
    Bayer[:, 0, 1:H2:2, 0:W2:2] = bggr[:, 2, :, :]  # G2
    Bayer[:, 0, 1:H2:2, 1:W2:2] = bggr[:, 3, :, :]  # R

    return Bayer

def bayer2bggr(Bayer):  # Bayer is BGGR here
    H = np.size(Bayer, 0)
    W = np.size(Bayer, 1)
    H2 = H // 2
    W2 = W // 2
    bggr = np.zeros((H2, W2, 4))

    bggr[:, :, 0] = Bayer[0:H:2, 0:W:2]  # B
    bggr[:, :, 1] = Bayer[0:H:2, 1:W:2]  # G1
    bggr[:, :, 2] = Bayer[1:H:2, 0:W:2]  # G2
    bggr[:, :, 3] = Bayer[1:H:2, 1:W:2]  # R

    return bggr


def rgb2bggr(rgb):
    h = np.size(rgb, 0)
    w = np.size(rgb, 1)
    h2 = h
    w2 = w
    bggr = np.zeros((h2, w2))

    bggr[0:h2:2, 0:w2:2] = rgb[0:h2:2, 0:w2:2, 0]  # B
    bggr[0:h2:2, 1:w2:2] = rgb[0:h2:2, 1:w2:2, 1]  # G1
    bggr[1:h2:2, 0:w2:2] = rgb[1:h2:2, 0:w2:2, 1]  # G2
    bggr[1:h2:2, 1:w2:2] = rgb[1:h2:2, 1:w2:2, 2]  # R

    return bggr


def norm_bayer(
    Bayer: np.ndarray, w: np.ndarray, b: np.ndarray, fwd: bool = True
) -> np.ndarray:
    if fwd:
        return (Bayer - b) / (w - b)
    else:
        return Bayer * (w - b) + b


def center_crop(img: np.ndarray, width: int, height: int, channel: str):
    """Center crop an image"""
    if channel == "last":
        y = img.shape[0] // 2 - height // 2
        x = img.shape[1] // 2 - width // 2
        if len(img.shape) == 3:
            return img[y : y + height, x : x + width, :]
        return img[y: y+height, x: x+width]

    elif channel == "first":
        y = img.shape[1] // 2 - height // 2
        x = img.shape[2] // 2 - width // 2

        return img[:, y:y+height, x:x+width]


def functional_crop(img: np.ndarray, crop_params: list, channel: str):
    """Crop an image at given params
    """
    top = crop_params[0]
    left = crop_params[1]
    height = crop_params[2]
    width = crop_params[3]
    if channel == "last":
        return img[top: top+height, left: left+width, :]
    elif channel == "first":
        return img[:, top: top+height, left: left+width]


def save_image(
    path: str,
    img: np.ndarray,
    depth: int = 16,
    normalize: bool = False,
    save_np: bool = False,
    Gamma: float = 0.0,
    max_range: float = 1.0,
    inv_ltm: bool=False
):
    """Image saving function
    img: numpy array [0, 1] or [0, 255] floating point in RGB format
    depth: 8/16-bit int or 32-bit float
    normalize: bool
    HDR: if True applies gamma correction
    save_np: also save an npy raw version
    """
    if isinstance(img, torch.Tensor):
        img = torch_to_np(img)

    if img.shape[-1] == 4:
        img = colour_demosaicing.demosaicing_CFA_Bayer_Menon2007(bggr2bayer(img), pattern='BGGR')

    if inv_ltm:
        mu =1000
        img=(np.exp(np.log(1+mu)*np.clip(img,0,1))-1)/mu

    if max_range == 255:
        img = img / 255

    if normalize:
        img = img / img.max()

    if save_np:
        extension = path.split(".")[-1]
        np.save(path.strip(f".{extension}"), img)


    if depth == 16:
        if Gamma:
            img = np.uint16(np.clip(img, 0, 1) ** (1 / Gamma) * 65535)
        else:
            img = np.uint16(np.clip(img, 0, 1) * 65535)
        if not path.endswith(("png", "tiff", "jpg")):
            raise ValueError(
                "16 bit images have to be saved in png, tiff or jpg format"
            )
    elif depth == 8:
        if Gamma:
            img = np.uint8(np.clip(img, 0, 1) ** (1 / Gamma) * 255)
        else:
            img = np.uint8(np.clip(img, 0, 1) * 255)
    elif depth == 32:
        img = np.float32(img)
        if not path.endswith(("tiff", "exr")):
            raise ValueError("32 bit fp images need to be saved as tiff or exr")

    else:
        raise Exception("Depth must be one of 8, 16 or 32")

    return cv2.imwrite(path, cv2.cvtColor(img, cv2.COLOR_RGB2BGR))

#### Pyramid Gaussian Down
#### https://github.com/koshian2/swd-pytorch/blob/master/swd.py

# Gaussian blur kernel
def get_gaussian_kernel(device="cpu"):
    kernel = np.array([
        [1, 4, 6, 4, 1],
        [4, 16, 24, 16, 4],
        [6, 24, 36, 24, 6],
        [4, 16, 24, 16, 4],
        [1, 4, 6, 4, 1]], np.float32) / 256.0
    gaussian_k = torch.as_tensor(kernel.reshape(1, 1, 5, 5)).to(device)
    return gaussian_k

def pyramid_down(image, device="cpu"):
    gaussian_k = get_gaussian_kernel(device=device)        
    # channel-wise conv(important)
    multiband = [F.conv2d(image[:, i:i + 1,:,:], gaussian_k, padding=2, stride=2) for i in range(image.shape[1])]
    down_image = torch.cat(multiband, dim=1)
    return down_image

def gaussian_pyramid(original, n_pyramids, device="cpu"):
    x = original
    # pyramid down
    pyramids = [original]
    for i in range(n_pyramids-1):
        x = pyramid_down(x, device=device)
        pyramids.append(x)
    return pyramids


## AWB GrayWorld 
## https://bitbucket.org/sensebrain/nightsight/src/master/python/awb/awb_grayworld.py

def awb_grayworld(img: np.ndarray, saturation: float = 1.0):
    # remove values larger than saturation
    img2 = img[np.max(img) <= saturation].reshape((-1, 3))

    # mean values in each rgb channels
    mean_r = np.mean(img2[:, 0])
    mean_g = np.mean(img2[:, 1])
    mean_b = np.mean(img2[:, 2])

    ratio_r = mean_g / mean_r
    ratio_b = mean_g / mean_b

    out_img = np.zeros(img.shape)
    out_img[:, :, 0] = img[:, :, 0] * ratio_r
    out_img[:, :, 1] = img[:, :, 1]
    out_img[:, :, 2] = img[:, :, 2] * ratio_b

    out_img = np.clip(out_img, 0., 1.)
    return out_img


def net_desc(net):
	model_parameters = filter(lambda p: p.requires_grad, net.parameters())
	params = sum([np.prod(p.size()) for p in model_parameters])
	return net.__str__() + "\nTrainable parameters: {} \n".format(params)


def center_psf_mass(psf: np.ndarray):
    """
    psf: [0, 1] np.float32
    Uses center of mass analysis to center to PSF in the image
    Returns an np array with same dimensions 
    """
    print(f"PSF shape is {psf.shape}, center should be at {psf.shape[0] // 2, psf.shape[1] // 2}")

    #1. Threshold the image
    thresh = 1e-2
    psf_thresh = np.where(psf > thresh, 1.0, 0.0).astype(np.uint8) #Creates the "label image"

    #Calc regions with skimage
    region = skimage.measure.regionprops(psf_thresh, psf)[0]
    print(f"Current Center: row {region.centroid[0]} col {region.centroid[1]}")
    
    center_x = psf.shape[0] / 2
    center_y = psf.shape[1] / 2

    x_trans = center_x - region.centroid[0]
    y_trans = center_y - region.centroid[1]

    if x_trans or y_trans:
        
        print(f"PSF is off center by ({x_trans} {y_trans})")
      
        if len(psf.shape) == 3:
            psf = scipy.ndimage.shift(psf, (x_trans, y_trans, 0), mode='wrap')
        else:
            psf = scipy.ndimage.shift(psf, (x_trans, y_trans))

        print(f"NEW CENTER {psf.shape[0] // 2, psf.shape[1] // 2}")
    
    else: 
        print("PSF is at the CENTER")
    
    return psf

def color_balance(img: np.ndarray):
    
    img[:, :, 0] = img[:, :, 0] / (np.sum(img[:, :, 0]) / np.sum(img[:, :, 1]))
    img[:, :, 2] = img[:, :, 2] / (np.sum(img[:, :, 2]) / np.sum(img[:, :, 1]))
    img[:, :, 1] = img[:, :, 1] 

    return img