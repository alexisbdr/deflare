import torch
import torch.nn as nn
import torch.nn.functional as F

from .blocks import *
from .layers import *

class MBCNN(nn.Module):

	def __init__(self, 
		in_channels: int,
		depth: int,
		p_block: int, 
		multi=True):

		super(MBCNN, self).__init__()	
		d_list_a = (1, 2, 3, 2, 1)
		d_list_b = (1, 2, 3, 2, 1)
		d_list_c = (1, 2, 2, 2, 1)

		self.s2d = Space2Depth(block_size=2)

		#channels: depth*2 --> depth*2	
		self.pre_in = nn.Sequential(
			#nn.Conv2d(12, depth*2, kernel_size=3, padding='same'),
			Conv2DSamePad(12, depth*2, kernel_size=3),
			nn.ReLU(inplace=True),
			PreBlock(depth*2, depth, d_list_a, p_block)
		)

		#channels: depth*2 --> depth*2	
		self.down1 = nn.Sequential(
			nn.ZeroPad2d((1, 1, 1, 1)),
			#nn.Conv2d(depth*2, depth*2, kernel_size=3, padding='valid', stride=(2, 2)),
			nn.Conv2d(depth*2, depth*2, kernel_size=3, stride=(2, 2)),
			nn.ReLU(inplace=True),
			PreBlock(depth*2, depth, d_list_b, p_block)
		)

		self.down2 = nn.Sequential(
			nn.ZeroPad2d((1, 1, 1, 1)),
			#nn.Conv2d(depth*2, depth*2, kernel_size=3, padding='valid', stride=(2, 2)),
			nn.Conv2d(depth*2, depth*2, kernel_size=3, stride=(2, 2)),
			nn.ReLU(inplace=True),
			PreBlock(depth*2, depth, d_list_c, p_block),
			GlobalBlock(depth*2, depth),
			PosBlock(depth*2, depth, d_list_c),
			#nn.Conv2d(depth*2, 12, kernel_size=3, padding='same'),
			Conv2DSamePad(depth*2, 12, kernel_size=3),
			Depth2Space(block_size=2)
		)

		self.up1 = nn.Sequential(
			#nn.Conv2d(depth*2 + 3, depth*2, kernel_size=1, padding='same'),
			Conv2DSamePad(depth*2+3, depth*2, kernel_size=1),
			nn.ReLU(inplace=True),
			GlobalBlock(depth*2, depth),
			PreBlock(depth*2, depth, d_list_b, p_block),
			GlobalBlock(depth*2, depth),
			PosBlock(depth*2, depth, d_list_b),
			#nn.Conv2d(depth*2, 12, kernel_size=3, padding='same'),
			Conv2DSamePad(depth*2, 12, kernel_size=3),
			Depth2Space(block_size=2)
		)

		self.up2 = nn.Sequential(
			#nn.Conv2d(depth*2 + 3, depth*2, kernel_size=1, padding='same'), #Difficult to know the actual dimensions of the input here
			Conv2DSamePad(depth*2 + 3, depth*2, kernel_size=1),
			nn.ReLU(inplace=True), 
			GlobalBlock(depth*2, depth),
			PreBlock(depth*2, depth, d_list_a, p_block),
			GlobalBlock(depth*2, depth),
			PosBlock(depth*2, depth, d_list_a),
			#nn.Conv2d(depth*2, 12, kernel_size=3, padding='same'),
			Conv2DSamePad(depth*2, 12, kernel_size=3),
			Depth2Space(block_size=2)
		)

	def forward(self, x, multi = False):

		print(f"MULTI {multi}")	
		_x = self.s2d(x)
		x1 = self.pre_in(_x)
		x2 = self.down1(x1)
		x3 = self.down2(x2)
		_x2 = torch.cat((x3, x2), axis=1)
		_x2 = self.up1(_x2)
		_x1 = torch.cat((x1, _x2), axis=1)
		y = self.up2(_x1)

		output_list = [x3, _x2, y]

		if multi:
			return output_list
		return y


def test_MBCNN():
	device = 'cuda' if torch.cuda.is_available() else 'cpu' 
	_in = torch.zeros((2, 3, 224, 224))
	_in = _in.to(device)
	model_64 = MBCNN(3, 64)
	model_64 = model_64.to(device)
	model_32 = MBCNN(3, 32)
	model_32 = model_32.to(device)
	res_64 = model_64(_in)
	print(f"Res 64 {res_64.shape}")
	res_32 = model_32(_in)
	print(f"Res 32 {res_32.shape}")


if __name__ == "__main__":

	test_MBCNN()
