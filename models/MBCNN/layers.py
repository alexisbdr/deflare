import math

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import matplotlib.pyplot as plt

### Pytorch Implementation of depth2space and space2depth
### see reference here: https://discuss.pytorch.org/t/is-there-any-layer-like-tensorflows-space-to-depth-function/3487/14

class Space2Depth(nn.Module):

	def __init__(self, block_size, **kwargs):
		super(Space2Depth, self).__init__()
		self.bs = block_size
	
	def forward(self, x):
		N, C, H, W = x.size()
		x = x.view(N, C, H // self.bs, self.bs, W // self.bs, self.bs)  # (N, C, H//bs, bs, W//bs, bs)
		x = x.permute(0, 3, 5, 1, 2, 4).contiguous()  # (N, bs, bs, C, H//bs, W//bs)
		x = x.view(N, C * (self.bs ** 2), H // self.bs, W // self.bs)  # (N, C*bs^2, H//bs, W//bs)
		return x

class Depth2Space(nn.Module):

	def __init__(self, block_size):
		super().__init__()
		self.bs = block_size

	def forward(self, x):
		N, C, H, W = x.size()
		x = x.view(N, self.bs, self.bs, C // (self.bs ** 2), H, W)  # (N, bs, bs, C//bs^2, H, W)
		x = x.permute(0, 3, 4, 1, 5, 2).contiguous()  # (N, C//bs^2, H, bs, W, bs)
		x = x.view(N, C // (self.bs ** 2), H * self.bs, W * self.bs)  # (N, C//bs^2, H * bs, W * bs)
		return x


def get_IDCT8():
	
	kernel = torch.zeros((64, 64, 1, 1), dtype=torch.float32)
	r1 = math.sqrt(1.0/8.0)
	r2 = math.sqrt(1.0/8.0)
	for i in range(8):
		_u = 2*i+1
		for j in range(8):
			_v = 2*j+1
			index = i*8+j
			for u in range(8):
				for v in range(8):
					index2 = u*8+v
					t = math.cos(_u*u*math.pi/16)*math.cos(_v*v*math.pi/16)
					t = t*r1 if u==0 or v==0 else t*r2
					kernel[index, index2, 0, 0] = t

	return kernel

def get_parametrized_IDCT(p: int = 8):
	
	kernel = torch.zeros((p**2, p**2, 1, 1), dtype=torch.float32)
	weights = torch.zeros((p**2, 1, 1, 1), dtype=torch.float32)

	r1 = math.sqrt(1.0/p)
	r2 = math.sqrt(1.0/p)
	for i in range(p):
		_u = 2*i+1
		for j in range(p):
			_v = 2*j+1
			index = i*p+j
			for u in range(p):
				for v in range(p):
					index2 = u*p+v
					t = math.cos(_u*u*math.pi/(2*p))*math.cos(_v*v*math.pi/(2*p))
					t = t*r1 if u==0 or v==0 else t*r2
					kernel[index, index2, 0, 0] = t
	
	return kernel, weights

			
class AdaptiveImplicitLayers(nn.Module):

	def __init__(self, p_block: int):
		super(AdaptiveImplicitLayers, self).__init__()

		kernel, weights = get_parametrized_IDCT(p_block)
		self.kernel = Variable(kernel)
		self.it_weights = nn.Parameter(weights, requires_grad=True)

		self._reversed_padding_repeated_twice = [0, 0] * 1
		for d, k, i in zip((1, 1), (1, 1),
							range(1 - 1, -1, -1)):
			total_padding = d * (k - 1)
			left_pad = total_padding // 2
			self._reversed_padding_repeated_twice[2 * i] = left_pad
			self._reversed_padding_repeated_twice[2 * i + 1] = (
				total_padding - left_pad)

	def forward(self, x):
		
		with torch.no_grad():
			self.kernel = self.kernel.to(self.it_weights.device)
			self.kernel = torch.mul(self.kernel, self.it_weights)
		y = F.conv2d(
			F.pad(x, self._reversed_padding_repeated_twice), self.kernel
		)

		return y

class LBFLayer(nn.Module):
	def __init__(self, 
		in_channels: int,
		out_channels: int,
		p_block: int):
		super(LBFLayer, self).__init__()
		self.conv_in = Conv2DSamePad(in_channels, p_block**2, kernel_size=3)
		self.adaptive = AdaptiveImplicitLayers(p_block)
		self.conv_out = Conv2DSamePad(p_block**2, out_channels, kernel_size=3)

	def forward(self, x):

		x = self.conv_in(x)
		x = self.adaptive(x)
		x = self.conv_out(x)

		return x


class ScaleLayer(nn.Module):
	def __init__(self, scale):
		super(ScaleLayer, self).__init__()

		weight = torch.ones((1, )) * scale
		self.scale = nn.Parameter(weight, requires_grad = True)
	
	def forward(self, x):
		return x * self.scale


class WeightNegativeConstraint(object):

	def __init__(self):
		pass

	def __call__(self, module):
		if hasattr(module, 'weight'):
			w = module.weight.data
			w = torch.max(w, 0)
			module.weight.data = w

class GlobalAvgPool2d(nn.Module):

	def __init__(self):
		super(GlobalAvgPool2d, self).__init__()

	def forward(self, x):
		"""Averages over H, W Dims
		Args: 
			x: (N, C, H, W) torch tensor
		"""

		return torch.mean(x, axis=[2, 3])

class Conv2DSamePad(nn.Conv2d):
	def __init__(self, *args, **kwargs):
		
		super(Conv2DSamePad, self).__init__(*args, **kwargs)

		### Implementation of "same" padding from pytorch:
		### https://github.com/pytorch/pytorch/blob/master/torch/nn/modules/conv.py#L304
		self._reversed_padding_repeated_twice = [0, 0] * len(self.kernel_size)
		for d, k, i in zip(self.dilation, self.kernel_size,
							range(len(self.kernel_size) - 1, -1, -1)):
			total_padding = d * (k - 1)
			left_pad = total_padding // 2
			self._reversed_padding_repeated_twice[2 * i] = left_pad
			self._reversed_padding_repeated_twice[2 * i + 1] = (
				total_padding - left_pad)

	def forward(self, x):
		if int(torch.__version__.split('.')[1]) >= 8:
			return self._conv_forward(
				F.pad(x, self._reversed_padding_repeated_twice), self.weight, bias=self.bias) # PyTorch 1.9
		else:
			return self._conv_forward(
				F.pad(x, self._reversed_padding_repeated_twice), self.weight) # Pytorch 1.5


def test_IDCT_kernels():

	IDCT8 = get_IDCT8().squeeze(-1)
	plt.imshow(IDCT8)
	plt.show()

	IDCT8_param = get_parametrized_IDCT(8).squeeze(-1)
	plt.imshow(IDCT8_param)
	plt.show()

	IDCT16 = get_parametrized_IDCT(16).squeeze(-1)
	plt.imshow(IDCT16)
	plt.show()


if __name__ == "__main__":
	test_IDCT_kernels()
