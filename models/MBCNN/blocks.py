import torch
import torch.nn as nn
import torch.nn.functional as F

from .layers import *

class PreBlock(nn.Module):

	def __init__(self,
		in_channels: int,
		out_channels: int,
		d_list: list,
		p_block: int,
		enable=True):
	
		super(PreBlock, self).__init__()
		self.in_channels = in_channels
		self.out_channels = out_channels
		self.d_list = d_list
		self.p_block = p_block

		self.dilated_block = nn.ModuleList()
		for i, d in enumerate(d_list):
			self.dilated_block.append(
				nn.Sequential(
					#nn.Conv2d(in_channels, out_channels, kernel_size=3, dilation=d, padding='same'),
				Conv2DSamePad(in_channels, out_channels, kernel_size=3, dilation=d),
				nn.ReLU(inplace=True)
				)
			)
			in_channels += out_channels
		#self.conv1 = nn.Conv2d(in_channels, 64, kernel_size=3, padding='same')
		self.conv1 = Conv2DSamePad(in_channels, p_block**2, kernel_size=3)
		self.adaptive = AdaptiveImplicitLayers(p_block)
		#self.conv2 = nn.Conv2d(64, out_channels*2, kernel_size=1, padding='same')
		self.conv2 = Conv2DSamePad(p_block**2, out_channels*2, kernel_size=1)
		self.scale = ScaleLayer(0.1)
		
	def forward(self, x):

		in_x = x
		for conv_relu in self.dilated_block:
			x_out = conv_relu(x)
			x = torch.cat((x, x_out), axis=1)
		x = self.conv1(x)
		x = self.adaptive(x)
		x = self.conv2(x)
		x = self.scale(x)
		x = x.add_(in_x)

		return x


class FTRB(PreBlock):

	def __init__(self,
		*args, **kwargs):

		super(FTRB, self).__init__(*args, **kwargs)

		self.conv2 = Conv2DSamePad(self.p_block**2, self.out_channels, kernel_size=1)
		
	def forward(self, x):

		in_x = x
		for conv_relu in self.dilated_block:
			x_out = conv_relu(x)
			x = torch.cat((x, x_out), axis=1)
		x = self.conv1(x)
		x = self.adaptive(x)
		x = self.conv2(x)
		x = self.scale(x)
		x = x.add_(in_x)

		return x


class PosBlock(nn.Module):

	def __init__(self,
		in_channels: int,
		out_channels: int,
		d_list: list):

		super(PosBlock, self).__init__()
		self.dilated_block = nn.ModuleList()
		for i, d in enumerate(d_list):
			self.dilated_block.append(
				nn.Sequential(
					#nn.Conv2d(in_channels, out_channels, kernel_size=3, dilation=d, padding='same'),
					Conv2DSamePad(in_channels, out_channels, kernel_size=3, dilation=d),
					nn.ReLU(inplace=True)
				)
			)
			in_channels += out_channels
		self.conv_out = nn.Sequential(
			#nn.Conv2d(in_channels, out_channels * 2, kernel_size=1, padding='same'),
			Conv2DSamePad(in_channels, out_channels * 2, kernel_size=1),
			nn.ReLU(inplace=True)
		)

	def forward(self, x):

		for conv_relu in self.dilated_block:
			x_out = conv_relu(x)
			x = torch.cat((x_out, x), axis=1)
		x = self.conv_out(x)

		return x


class GlobalBlock(nn.Module):

	def __init__(self,
		in_channels: int,
		out_channels: int):

		super(GlobalBlock, self).__init__()
		self.in_block = nn.Sequential(
			nn.ZeroPad2d((1, 1, 1, 1)),
			nn.Conv2d(in_channels, out_channels*4, kernel_size=3, stride=(2, 2)),
			nn.ReLU(inplace=True),
			GlobalAvgPool2d(),
			nn.Linear(out_channels*4, out_channels*16),
			nn.ReLU(inplace=True),
			nn.Linear(out_channels*16, out_channels*8),
			nn.ReLU(inplace=True),
			nn.Linear(out_channels*8, out_channels*4),
		)
		
		self.conv_in = nn.Sequential(
			#nn.Conv2d(in_channels, out_channels*4, kernel_size=1, padding='same'),
			Conv2DSamePad(in_channels, out_channels*4, kernel_size=1),
			nn.ReLU(inplace=True)
		)

		self.conv_out = nn.Sequential(
			#nn.Conv2d(out_channels*4, out_channels*2, kernel_size=1, padding='same'),
			Conv2DSamePad(out_channels*4, out_channels*2, kernel_size=1),
			nn.ReLU(inplace=True)
		)
	
	def forward(self, x):

		block_x = self.in_block(x)
		x = self.conv_in(x)
		in_x = torch.mul(x, block_x.view(block_x.shape[0], -1, 1, 1))
		out = self.conv_out(in_x)

		return out


def test_pre_block():

	_in = torch.randn((2, 128, 224, 224))
	d_list = (1, 2, 3, 2, 1)
	block = PreBlock(128, 64, d_list)
	res = block(_in)
	print(f"Final shape: {res.shape}")

def test_pos_block():
	
	_in = torch.ones((2, 128, 224, 224))
	d_list = (1, 2, 2, 2, 1)
	block = PosBlock(128, 64, d_list)
	res = block(_in)
	print(f"FINAL SHAPE: {res.shape}")

def test_global_block():

	_in = torch.ones((1, 128, 224, 224))
	block = GlobalBlock(128, 64)
	res = block(_in)
	print(f"FINAL SHAPE {res.shape}")


if __name__ == "__main__":
	test_pre_block()
