import torch
import torch.nn as nn
import numpy as np
import math

#############################################################           the func is the model definition to SR from quad to bayer       ######################################




def get_upsample_filter(size):
    """Make a 2D bilinear kernel suitable for upsampling"""
    factor = (size + 1) // 2
    if size % 2 == 1:
        center = factor - 1
    else:
        center = factor - 0.5
    og = np.ogrid[:size, :size]
    filter = (1 - abs(og[0] - center) / factor) * \
             (1 - abs(og[1] - center) / factor)
    return torch.from_numpy(filter).float()




def conv1(inp, oup, ksz,stride):
    return nn.Sequential(
        nn.Conv2d(in_channels = inp, out_channels = oup, kernel_size=3, stride=stride, padding= 1, bias=False),
        nn.PReLU()

    )


def conv_dw(inp, oup, stride):

    return nn.Sequential(
        nn.Conv2d(inp, inp, 3, stride, 1, groups=inp, bias=False),
        nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
        nn.PReLU()

    )

###############################################################                     CONV BLOCK
class _Conv_Block(nn.Module):

    def __init__(self,in_channels,out_channels,nc):
        super(_Conv_Block, self).__init__()
        kernel_size = 3,
        self.cov_block = nn.Sequential(

            # conv_dw(in_channels, nc, 1),
            conv1(in_channels, nc, kernel_size , 1),

            conv1(nc, nc, kernel_size ,  1),
            conv1(nc, nc, kernel_size ,  1),
            conv1(nc, nc, kernel_size ,  1),
            conv1(nc, nc, kernel_size ,  1),
            conv1(nc, nc, kernel_size ,  1),

            conv1(nc, nc, kernel_size, 1),
            conv1(nc, nc, kernel_size, 1),
            conv1(nc, nc, kernel_size, 1),
            conv1(nc, nc, kernel_size, 1),
            conv1(nc, nc, kernel_size, 1),

            conv1(nc,out_channels,kernel_size,1)
        )


    def forward(self, x):
        output = self.cov_block(x)
        return output


class _Conv_Block3(nn.Module):

    def __init__(self,in_channels,out_channels,nc):
        super(_Conv_Block3, self).__init__()
        kernel_size = 3,
        self.cov_block = nn.Sequential(

            # conv_dw(in_channels, nc, 1),
            conv1(in_channels, nc, kernel_size , 1),

            conv1(nc, nc, kernel_size ,  1),
            conv1(nc, nc, kernel_size ,  1),
            conv1(nc, nc, kernel_size ,  1),

            conv1(nc,out_channels,kernel_size,1)
        )


    def forward(self, x):
        output = self.cov_block(x)
        return output

class remosaicNet(nn.Module):


    #[in]
    #   in_channels: the num of input channels
    #   out_channels: the num of output channels
    #   conv_block_channels: the number of intermediate channels in the conv_block
    #
    def __init__(self, in_channels, out_channels,conv_block_channels=64):
        super(remosaicNet, self).__init__()


        block =_Conv_Block(in_channels,out_channels, conv_block_channels)
        self.convt_F1 = self.make_layer(block)



        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            if isinstance(m, nn.ConvTranspose2d):
                c1, c2, h, w = m.weight.data.size()
                weight = get_upsample_filter(h)
                m.weight.data = weight.view(1, 1, h, w).repeat(c1, c2, 1, 1)
                if m.bias is not None:
                    m.bias.data.zero_()



    def make_layer(self, block):
        layers = []
        layers.append(block)
        return nn.Sequential(*layers)


    def forward(self, x):


        convt_F1 = self.convt_F1(x)

        return convt_F1



class remosaicNet3(nn.Module):


    #[in]
    #   in_channels: the num of input channels
    #   out_channels: the num of output channels
    #   conv_block_channels: the number of intermediate channels in the conv_block
    #
    def __init__(self, in_channels, out_channels,conv_block_channels=64):
        super(remosaicNet3, self).__init__()


        block =_Conv_Block3(in_channels,out_channels, conv_block_channels)
        self.convt_F1 = self.make_layer(block)



        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            if isinstance(m, nn.ConvTranspose2d):
                c1, c2, h, w = m.weight.data.size()
                weight = get_upsample_filter(h)
                m.weight.data = weight.view(1, 1, h, w).repeat(c1, c2, 1, 1)
                if m.bias is not None:
                    m.bias.data.zero_()



    def make_layer(self, block):
        layers = []
        layers.append(block)
        return nn.Sequential(*layers)


    def forward(self, x):


        convt_F1 = self.convt_F1(x)

        return convt_F1


