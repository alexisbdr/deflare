import numpy
import cv2
from colour_demosaicing.bayer.demosaicing import bilinear, malvar2004, menon2007

def image_demosaic(rgb_raw, demosaic_method):

    if demosaic_method == 'bilinear':
        rgb1 = bilinear.demosaicing_CFA_Bayer_bilinear(rgb_raw, pattern='BGGR')
    if demosaic_method == 'M2004':
        rgb1 = malvar2004.demosaicing_CFA_Bayer_Malvar2004(rgb_raw, pattern='BGGR')
    if demosaic_method == 'M2007':
        rgb1 = menon2007.demosaicing_CFA_Bayer_Menon2007(rgb_raw, pattern='BGGR')

    return rgb1


def Bayer2bggr(Bayer):  # Bayer is BGGR here
    H = numpy.size(Bayer, 0)
    W = numpy.size(Bayer, 1)
    H2 = H // 2
    W2 = W // 2
    bggr = numpy.zeros((H2, W2, 4))

    bggr[:, :, 0] = Bayer[0:H:2, 0:W:2]  # B
    bggr[:, :, 1] = Bayer[0:H:2, 1:W:2]  # G1
    bggr[:, :, 2] = Bayer[1:H:2, 0:W:2]  # G2
    bggr[:, :, 3] = Bayer[1:H:2, 1:W:2]  # R

    return bggr

def RGB2Bayer(rgb):  #Bayer is BGGR here
    H=numpy.size(rgb,0)
    W=numpy.size(rgb,1)
    H2=H//2*2
    W2=W//2*2
    bayer=numpy.zeros((H2,W2))
    bayer[0:H2:2, 0:W2:2] = rgb[0:H2:2, 0:W2:2, 2] #B
    bayer[0:H2:2, 1:W2:2] = rgb[0:H2:2, 1:W2:2, 1]  # G1
    bayer[1:H2:2, 0:W2:2] = rgb[1:H2:2, 0:W2:2, 1]  # G2
    bayer[1:H2:2, 1:W2:2] = rgb[1:H2:2, 1:W2:2, 0] #R

    return bayer

def bggr2Bayer (bggr):
    H=numpy.size(bggr,0)
    W=numpy.size(bggr,1)
    H2=H*2
    W2=W*2
    Bayer=numpy.zeros((H2,W2))

    Bayer[0:H2:2, 0:W2:2] = bggr[:, :, 0]  #B
    Bayer[0:H2:2, 1:W2:2] = bggr[:, :, 1]  # G1
    Bayer[1:H2:2, 0:W2:2] = bggr[:, :, 2]  # G2
    Bayer[1:H2:2, 1:W2:2] = bggr[:, :, 3]  #R

    return Bayer


def Bayer2halfRGB(Bayer): #Bayer is BGGR here
    H=numpy.size(Bayer,0)
    W=numpy.size(Bayer,1)
    H2=H//2
    W2=W//2
    rgb=numpy.zeros((H2,W2,3))

    rgb[:, :, 0] = Bayer[1:H:2, 1:W:2]  # R
    rgb[:, :, 1] = (Bayer[0:H:2, 1:W:2] + Bayer[1:H:2, 0:W:2])/2  #(G1 + G2)/2
    rgb[:, :, 2] = Bayer[0:H:2, 0:W:2]  #B

    return rgb

def gamma_correction(img, gamma=1/2.2):
    img = numpy.clip(img,0,1)**gamma
    return img

def rgb2gray(rgb_img):
    """Grayscale image"""
    gray_img = numpy.dot(rgb_img[..., :3], [0.299, 0.587, 0.114])
    return gray_img

def contrast(grayscale):
    """Function that returns the Constrast numpy array"""
    h,w = grayscale.shape[0], grayscale.shape[1]
    contrast = numpy.zeros((h, w))
    gray_extended = numpy.zeros((h + 2, w + 2))
    gray_extended[1:h + 1, 1:w + 1] = grayscale.copy()
    #        kernel = numpy.array([[ -1, -1, -1 ],
    #                           [ -1, 8, -1 ],
    #                            [ -1, -1, -1 ]])
    kernel = numpy.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]])
    for row in range(h):
        for col in range(w):
            contrast[row][col] = numpy.abs(
                (kernel *
                 gray_extended[row:(row + 3), col:(col + 3)]).sum())
    contrast = (contrast - numpy.min(contrast))
    contrast = contrast / numpy.max(contrast)
    return contrast

def thresholding(input_img, thresh):
    _, threshold_img = cv2.threshold(input_img, thresh, 255, cv2.THRESH_BINARY)
    return threshold_img

### Adaptive thresholding
def adaptive_thresholding(input_img, block_size, c):
    threshold_img = cv2.adaptiveThreshold((input_img).astype(numpy.uint8),255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,block_size,c)
    return threshold_img

# Otsu's thresholding after Gaussian filtering
def otsu_thresholding(input_img, kernel_size, otsu_thresh):
    blur = cv2.GaussianBlur((input_img).astype(numpy.uint8),kernel_size,0)
    _,threshold_img = cv2.threshold(blur,otsu_thresh,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    return threshold_img

def generate_masks(flare_bggr, threshold_img, combine):
    #locate strong source based on intensity
    mask = numpy.zeros(flare_bggr.shape)
    mask[numpy.where(flare_bggr > 1.5)] = 1
    mask_intensity = numpy.clip(numpy.sum(mask, axis=2), 0, 1)
    threshold_intensity = threshold_img*mask_intensity
    mask_intensity = threshold_intensity

    # dilate the mask
    mask_c = cv2.GaussianBlur(mask_intensity, (101, 101), cv2.BORDER_DEFAULT)
    thres = 0
    mask_dil = numpy.zeros(mask_c.shape)
    mask_dil[numpy.where(mask_c > thres)] = 1
    
    # smoothen the mask
    mask_smooth = cv2.GaussianBlur(mask_dil, (41, 41), 21, cv2.BORDER_DEFAULT)
    
    return [mask_intensity, mask_dil, mask_smooth]

def rot90(img):
    return cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)

def get_edge_points_cv(contour,level):
    coord=contour[level]
    x_range=numpy.arange(numpy.amin(coord[:,:,1]),numpy.amax(coord[:,:,1])+1)
    y_range = numpy.arange(numpy.amin(coord[:,:,0]), numpy.amax(coord[:,:,0]) + 1)

    Y,X=numpy.meshgrid(y_range,x_range)

    return X,Y