import numpy
import cv2
import os
import sys
import numpy as np
from colour_demosaicing.bayer.demosaicing import bilinear, malvar2004, menon2007
import argparse
import yaml

sys.path.append('.')
from deflare.deflare import Deflare

def image_demosaic(rgb_raw, demosaic_method):

    if demosaic_method == 'bilinear':
        rgb1 = bilinear.demosaicing_CFA_Bayer_bilinear(rgb_raw, pattern='BGGR')
    if demosaic_method == 'M2004':
        rgb1 = malvar2004.demosaicing_CFA_Bayer_Malvar2004(rgb_raw, pattern='BGGR')
    if demosaic_method == 'M2007':
        rgb1 = menon2007.demosaicing_CFA_Bayer_Menon2007(rgb_raw, pattern='BGGR')

    return rgb1



if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--config_file', type=str, default='./config.yml')
    parser.add_argument('--input_folder', type=str, default='../_data/input/test_deflare')
    parser.add_argument('--output_folder', type=str, default='./_result/deflare_inference/test_result')

    args = parser.parse_args()

    # read yaml file
    with open(args.config_file, 'r') as stream:
        try:
            config = yaml.safe_load(stream)['deflare']
        except yaml.YAMLError as exc:
            print(exc)

    # load input data
    Bayer = np.fromfile(os.path.join(args.input_folder, 'deflare_input.bin'),dtype='float32')
    Bayer = numpy.reshape(Bayer, (1940, 2592))

    # read the mask
    mask_path=os.path.join(args.input_folder, 'mask.png')
    mask = cv2.imread(mask_path, 0).astype(np.float32)
    if len(mask.shape) > 2:
        mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)

    # output_folder
    os.makedirs(args.output_folder, exist_ok=True)

    Bayer = np.clip(Bayer, 0, 16)
   

    # plot input
    Dem = image_demosaic(Bayer, 'M2007')
    cv2.imwrite(os.path.join(args.output_folder, 'deflare_input.png'),
                cv2.cvtColor(numpy.uint8(numpy.clip(Dem, 0, 1) ** (1 / 2.2) * 255), cv2.COLOR_RGB2BGR))


   #deflare

    DF = Deflare(mask_method=config['mask_method'],
                flare_thres=config['flare_thres'], 
                small=config['small'],
                smooth_thres=config['smooth_thres'],
                mask=mask,
                gray_psf_path=config['gray_psf_path'],
                filter_path=config['filter_path'],
                dict_ckpt_path=config['dict_ckpt_path'])
    out_Bayer = DF.process(Bayer)

    # plot output

    Dem = image_demosaic(out_Bayer, 'M2007')
    cv2.imwrite(os.path.join(args.output_folder, 'deflare_output_large.png'),
                cv2.cvtColor(numpy.uint8(numpy.clip(Dem, 0, 1) ** (1 / 2.2) * 255), cv2.COLOR_RGB2BGR))









