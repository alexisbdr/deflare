

import argparse, os
import random
import numpy as np


import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn

from models.deflare.Lightweight_model.ResSkip_arch import ResSkipC_l, ResSkipC_l2
from models.deflare.remosaic_net import remosaicNet


############################################3               the main entry to test the lapsrn model          ##########################################

def main(input, checkpoint, device, small=False,batch_sz=64):
    
#############################################################3            load testing arguments               ############################################
    global opt, model

    if 0:
        opt = parser.parse_args()
    else:
        opt = argparse.ArgumentParser(description="PyTorch LapSRN")
        opt.batchSize = batch_sz
        opt.resume=""
        opt.start_epoch=1

    opt.seed = random.randint(1, 10000)
    # print("Random Seed: ", opt.seed)
    torch.manual_seed(opt.seed)
    if device.type == 'cuda':
        torch.cuda.manual_seed(opt.seed)

    cudnn.benchmark = True

#############################################################3            load models             ############################################

    # print("===> Building model")

    if small==False:
        model = remosaicNet(4,4,128)
        #model = ResSkipC_l(4, 64)
    else:
        model = ResSkipC_l2(4, 32)

    model.load_state_dict(torch.load(checkpoint,map_location=device))

#############################################################                   Setting up GPU

    # print("===> Setting Torch Device")

    model = model.to(device)

#############################################################3            start testing              ############################################

    # print("===> Testing")

    model.eval()

    input = input[np.newaxis, :, :, :]

    input = torch.from_numpy(input).float()
    input = input.permute(0, 3, 1, 2)

    input = input.to(device)
    
    ############################################               predict
    
    residual = model(input)
    hat=input+residual

    hat = hat.permute(0, 2, 3, 1)

    output = np.squeeze(hat.detach().cpu().numpy())

    return output

#############################################################3            end testing             ############################################


def DNN_inference_patch(img_bggr,checkpoint,small,patch_size,pad_size, device): #with pad input
    
    batch_sz = 1

    H1 = np.size(img_bggr, 0)-2*pad_size
    W1 = np.size(img_bggr, 1)-2*pad_size

    out_bggr = np.zeros((H1, W1, 4))

    ps1 = np.arange(pad_size, H1 + pad_size, patch_size)
    ps2 = np.arange(pad_size, W1 + pad_size, patch_size)

    for ii in range(len(ps1)):
        for jj in range(len(ps2)):
            x_l = ps1[ii] + np.arange(-pad_size, min(patch_size + pad_size, H1 + 2*pad_size - ps1[ii]))
            y_l = ps2[jj] + np.arange(-pad_size, min(patch_size + pad_size, W1 + 2*pad_size - ps2[jj]))
            Y_L, X_L = np.meshgrid(y_l, x_l)
            img_c = img_bggr[X_L, Y_L,:]

            output = main(img_c, checkpoint, device, small, batch_sz)

            x_h2 = x_l[0] + np.arange(0, len(x_l) - 2*pad_size)
            y_h2 = y_l[0] + np.arange(0, len(y_l) - 2*pad_size)
            Y_H2, X_H2 = np.meshgrid(y_h2, x_h2)

            out_bggr[X_H2, Y_H2, :] = output[pad_size:-pad_size, pad_size:-pad_size, :]

    return out_bggr





