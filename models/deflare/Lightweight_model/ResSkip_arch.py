import torch.nn as nn

class ResSkipC_l(nn.Module):
    def __init__(
        self, 
        in_channels=4,
        channels=64,
        kernel_size: int = 3):
        super(ResSkipC_l, self).__init__()

        self.convh = nn.Conv2d(in_channels, channels, 3, padding=1, stride=1,bias=False)
        self.reluh = nn.PReLU(num_parameters=channels)

        self.conv1_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu1_1 = nn.PReLU(num_parameters=channels)
        self.conv1_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.conv2_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu2_1 = nn.PReLU(num_parameters=channels)
        self.conv2_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.conv3_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu3_1 = nn.PReLU(num_parameters=channels)
        self.conv3_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.conv4_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu4_1 = nn.PReLU(num_parameters=channels)
        self.conv4_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.conv5_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu5_1 = nn.PReLU(num_parameters=channels)
        self.conv5_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.conv6_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu6_1 = nn.PReLU(num_parameters=channels)
        self.conv6_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.convt = nn.Conv2d(channels, in_channels, 3, padding=1, stride=1,bias=False)


    def forward(self, x):
        x_h = self.reluh(self.convh(x))

        x_r1 = self.conv1_2(self.relu1_1(self.conv1_1(x_h)))
        x1 = x_h + x_r1

        x_r2 = self.conv2_2(self.relu2_1(self.conv2_1(x1)))
        x2 = x1 + x_r2

        x_r3 = self.conv3_2(self.relu3_1(self.conv3_1(x2)))
        x3 = x2 + x_r3

        x_r4 = self.conv4_2(self.relu4_1(self.conv4_1(x3)))
        x4 = x3 + x_r4

        x_r5 = self.conv5_2(self.relu5_1(self.conv5_1(x4)))
        x5 = x4 + x_r5

        x_r6 = self.conv6_2(self.relu6_1(self.conv6_1(x5)))
        x6 = x5 + x_r6

        x_t = self.convt(x6)

        return x_t


class ResSkipC_l2(nn.Module):
    def __init__(self, in_channels=4,channels=32):
        super(ResSkipC_l2, self).__init__()

        self.convh = nn.Conv2d(in_channels, channels, 3, padding=1, stride=2,bias=False)
        self.reluh = nn.PReLU(num_parameters=channels)

        self.conv1_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu1_1 = nn.PReLU(num_parameters=channels)
        self.conv1_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.conv2_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu2_1 = nn.PReLU(num_parameters=channels)
        self.conv2_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.conv3_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu3_1 = nn.PReLU(num_parameters=channels)
        self.conv3_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.conv4_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu4_1 = nn.PReLU(num_parameters=channels)
        self.conv4_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.conv5_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu5_1 = nn.PReLU(num_parameters=channels)
        self.conv5_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.conv6_1 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)
        self.relu6_1 = nn.PReLU(num_parameters=channels)
        self.conv6_2 = nn.Conv2d(channels, channels, 3, padding=1, stride=1,bias=False)

        self.convt = nn.ConvTranspose2d(channels, in_channels, 4, padding=1, stride=2, bias=False)


    def forward(self, x):
        x_h = self.reluh(self.convh(x))

        x_r1 = self.conv1_2(self.relu1_1(self.conv1_1(x_h)))
        x1 = x_h + x_r1

        x_r2 = self.conv2_2(self.relu2_1(self.conv2_1(x1)))
        x2 = x1 + x_r2

        x_r3 = self.conv3_2(self.relu3_1(self.conv3_1(x2)))
        x3 = x2 + x_r3

        x_r4 = self.conv4_2(self.relu4_1(self.conv4_1(x3)))
        x4 = x3 + x_r4

        x_r5 = self.conv5_2(self.relu5_1(self.conv5_1(x4)))
        x5 = x4 + x_r5

        x_r6 = self.conv6_2(self.relu6_1(self.conv6_1(x5)))
        x6 = x5 + x_r6

        x_t = self.convt(x6)

        return x_t







