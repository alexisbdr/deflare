# Deflare module
import numpy as np
import cv2
from models.deflare.DNN_inference import DNN_inference_patch
from models.deflare.mask_detection import flare_mask_generate_M0, flare_mask_generate_M2
import torch


class Deflare:
    """Deflare module for Under display camera

    Attributes:
        input_Bayer: BGGR, [0,16], float32
    """

    def __init__(self, 
                 mask_method='M2', 
                 flare_thres=1.5,
                 small=False,
                 mask=None,
                 is_mask=False,
                 gray_psf_path='../Filter/gray_psf_256.npy',
                 max_range=16,
                 is_Wolfgang=True,
                 filter_path='../Filter/XM_Filter_5.npy',
                 dict_ckpt_path={'small': '../checkpoint/small.pth',
                                 'large': '../checkpoint/large.pth'},
                 device=None):
        
        self.tag = "Deflare"
        self.mask_method=mask_method
        self.flare_thres=flare_thres
        self.gray_psf=np.load(gray_psf_path)
        self.filter=np.load(filter_path)    #fixed
        self.pad_size=64
        self.patch_szie=128 #fixed
        self.small=small  # use small network or large network
        self.mask=mask
        self.is_mask=is_mask
        self.ckpt_path = dict_ckpt_path['small'] if self.small else dict_ckpt_path['large']
        self.device = device if device else torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        self.max_range=max_range
        self.Wolfgang=is_Wolfgang

    def get_tag(self):
        return self.tag

    def RGB2Bayer(self,rgb):  # Bayer is BGGR here
        H = np.size(rgb, 0)
        W = np.size(rgb, 1)
        H2 = H // 2 * 2
        W2 = W // 2 * 2
        bayer = np.zeros((H2, W2))
        bayer[0:H2:2, 0:W2:2] = rgb[0:H2:2, 0:W2:2, 2]  # B
        bayer[0:H2:2, 1:W2:2] = rgb[0:H2:2, 1:W2:2, 1]  # G1
        bayer[1:H2:2, 0:W2:2] = rgb[1:H2:2, 0:W2:2, 1]  # G2
        bayer[1:H2:2, 1:W2:2] = rgb[1:H2:2, 1:W2:2, 0]  # R

        return bayer

    def Bayer2bggr(self,Bayer):  # Bayer is BGGR here
        H = np.size(Bayer, 0)
        W = np.size(Bayer, 1)
        H2 = H // 2
        W2 = W // 2
        bggr = np.zeros((H2, W2, 4))

        bggr[:, :, 0] = Bayer[0:H:2, 0:W:2]  # B
        bggr[:, :, 1] = Bayer[0:H:2, 1:W:2]  # G1
        bggr[:, :, 2] = Bayer[1:H:2, 0:W:2]  # G2
        bggr[:, :, 3] = Bayer[1:H:2, 1:W:2]  # R

        return bggr

    def bggr2Bayer(self,bggr):
        H = np.size(bggr, 0)
        W = np.size(bggr, 1)
        H2 = H * 2
        W2 = W * 2
        Bayer = np.zeros((H2, W2))

        Bayer[0:H2:2, 0:W2:2] = bggr[:, :, 0]  # B
        Bayer[0:H2:2, 1:W2:2] = bggr[:, :, 1]  # G1
        Bayer[1:H2:2, 0:W2:2] = bggr[:, :, 2]  # G2
        Bayer[1:H2:2, 1:W2:2] = bggr[:, :, 3]  # R

        return Bayer

    def convolve2d(self,a, K):
        '''
        convolve the image a with kernel K
        '''
        Fa = np.fft.fft2(a)
        FK = np.fft.fft2(K)

        return np.real(np.fft.fftshift(np.fft.ifft2(Fa * FK)))

    def Wolfgang_conv(self, Filter_bggr, flare_bggr, pad_size):

        Filter_bggr = np.pad(Filter_bggr, ([pad_size, pad_size], [pad_size, pad_size], [0, 0]), 'constant')
        flare_bggr = np.pad(flare_bggr, ([pad_size, pad_size], [pad_size, pad_size], [0, 0]), 'symmetric')

        out_bggr = np.zeros(flare_bggr.shape)

        print(Filter_bggr.shape)
        print(flare_bggr.shape)

        for c in range(4):
            Filter_bggr[:, :, c] = Filter_bggr[:, :, c] / Filter_bggr[:, :, c].sum()
            out_bggr[:, :, c] = self.convolve2d(Filter_bggr[:, :, c], flare_bggr[:, :, c])

        return out_bggr

    def process(self,input_Bayer):
        
        if np.amax(input_Bayer) <= self.flare_thres:
            print('No flare')
            out_Bayer = input_Bayer.copy()
            H,W=input_Bayer.shape[0], input_Bayer.shape[1]
            blending_mask=np.zeros((H//2,W//2))

        else:
            # flare detection and mask generation

            # pad Bayer to compensate for the fusion artifacts at the edge
            input_bggr = self.Bayer2bggr(input_Bayer)
            print(f"Input BGGR {input_bggr.shape}")
            flare_bggr = np.pad(input_bggr[2:-2, 2:-2, :], ([2, 2], [2, 2], [0, 0]), 'edge')
            input_Bayer = self.bggr2Bayer(flare_bggr)

            #pad the boundary to avoid the dark pixels generated in the previous steps


            if self.mask_method=='M2':
                blending_mask,_= flare_mask_generate_M2(input_Bayer, self.gray_psf, self.is_mask, self.mask, flare_thres=self.flare_thres)
            else:
                blending_mask, _ = flare_mask_generate_M0(flare_bggr, self.is_mask,  self.mask, flare_thres=self.flare_thres)

            # load filter

            #pad or crop the filter to match the size of Bayer
            Hf,Wf=self.filter.shape[0], self.filter.shape[1]
            Hb,Wb=input_Bayer.shape[0], input_Bayer.shape[1]

            if Hf<Hb and Wf<Wb: #filter size < image size, pad
                pad_h = (Hb - Hf) // 2
                pad_w = (Wb - Wf) // 2
                filter_resize=np.pad(self.filter,([pad_h,pad_h],[pad_w,pad_w],[0,0]),'constant')

            elif Hf<Hb and Wf>=Wb:
                pad_h = (Hb - Hf) // 2
                edge_w = (Wf - Wb) // 2
                filter_resize = np.pad(self.filter, ([pad_h, pad_h], [0, 0], [0, 0]), 'constant')
                filter_resize = filter_resize[:, edge_w:-edge_w, :]

            elif Hf>=Hb and Wf<Wb:
                pad_w = (Wb - Wf) // 2
                edge_h = (Hf - Hb) // 2
                filter_resize = np.pad(self.filter, ([0, 0], [pad_w,pad_w], [0, 0]), 'constant')
                filter_resize = filter_resize[edge_h:-edge_h, :, :]

            else: #filter size > image size, crop
                edge_h = (Hf - Hb) // 2
                edge_w = (Wf - Wb) // 2
                filter_resize=self.filter[edge_h:(Hf-edge_h),edge_w:(Wf-edge_w),:]


            Filter_bggr= self.Bayer2bggr(self.RGB2Bayer(filter_resize))

            # Wolfgang convolution in Bayer domain, with pad to avoid boundary artifacts

            if self.Wolfgang==True:
                Wolf_bggr =self.Wolfgang_conv(Filter_bggr, flare_bggr, pad_size=self.pad_size)
            else:#skip wolfgang
                Wolf_bggr = np.pad(flare_bggr, ([self.pad_size, self.pad_size], [self.pad_size, self.pad_size], [0, 0]), 'symmetric')

            # DNN
            Wolf_bggr=np.clip(Wolf_bggr,0,self.max_range)/self.max_range
            mu=1000
            Wolf_bggr=np.log(1+mu*Wolf_bggr)/np.log(1+mu)  #log rule tonemapping

            checkpoint = self.ckpt_path
            out_bggr = DNN_inference_patch(Wolf_bggr, checkpoint,small=self.small,patch_size=128,pad_size=self.pad_size, device=self.device)

            out_bggr=(np.exp(np.log(1+mu)*np.clip(out_bggr,0,1))-1)/mu  #inverse log rule
            out_bggr=self.max_range*np.clip(out_bggr,0,1)

            # blending with the original non-flare region
            for c in range(4):
                out_bggr[:, :, c] = blending_mask * out_bggr[:, :, c] + (1 - blending_mask) * flare_bggr[:, :, c]

            out_Bayer=self.bggr2Bayer(out_bggr)

        return out_Bayer,blending_mask

