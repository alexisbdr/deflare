import numpy
import cv2
from models.deflare.utils import *


def psf_conv(rgb, mask, gray_half_psf, fr=10000):
    gray = rgb2gray(rgb)
    # Grayscale regions with point light source
    flare_source_gray = gray.copy()
    flare_source_gray[numpy.where(mask<1)] = 0

    gray_psf_convolve = cv2.filter2D(flare_source_gray/255.,-1,numpy.clip(gray_half_psf*fr, 0, 1))
    return numpy.clip(gray_psf_convolve, 0, 1)

def flare_mask_generate_M0(flare_bggr,is_mask,mask,flare_thres=1.5):
    mask_for_intensitymap = numpy.zeros(flare_bggr.shape)
    mask_for_intensitymap[numpy.where(flare_bggr > flare_thres)] = 1
    mask_s = numpy.clip(numpy.sum(mask_for_intensitymap, axis=2), 0, 1)

    # dilation, expand the flare region to cover the entire flare
    mask_c = cv2.GaussianBlur(mask_s, (101, 101), cv2.BORDER_DEFAULT)
    mask_c2 = numpy.zeros(mask_c.shape)
    mask_c2[numpy.where(mask_c > 0)] = 1

    # load portrait mask

    if is_mask==True:  #use portrait mask to protect
        face = numpy.float16(mask)
        face2 = numpy.ones(face.shape)
        face2[numpy.where(face > 0.3)] = 0

        mask_c2 *= face2  # exclude face region


    # smooth the mask (for blending purpose)
    mask_d = cv2.GaussianBlur(mask_c2, (41, 41), 21, cv2.BORDER_DEFAULT)

    # binarize the mask
    weight_thres = 0.1
    mask_d2 = numpy.zeros(mask_d.shape)
    mask_d2[numpy.where(mask_d > weight_thres)] = 1

    # find the contours  [group the connected regions]
    contours, hierarchy = cv2.findContours(numpy.uint8(mask_d2 * 255), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    L = len(contours)  # number of connected regions

    # generate bounding box for each connected region, for patch based convoluton
    mask_label_2 = numpy.zeros(mask_d2.shape)
    for idx in range(L):
        X, Y = get_edge_points_cv(contours, idx)  # X,Y indicate the edges of the region
        mask_label_2[X, Y] = (idx + 1)

    return mask_d,  mask_label_2

def flare_mask_generate_M2(Bayer, gray_half_psf,is_mask, mask, flare_thres=1.5,thresh_block_size=3, thresh_c=0, psf_fr=10000, psf_conv_thresh=0.15,
                           psf_blur_ksize=21, smooth_ksize=41, smooth_sigma=21):
    original_Bayer = Bayer.copy()
    Bayer = (numpy.clip(Bayer, 0, 16)) / 16.

    # masks using contrast map
    rgb = Bayer2halfRGB(Bayer)
    gray = rgb2gray(rgb)
    rgb_contrast = contrast(gray)
    rgb = (rgb * 255.).astype(numpy.uint8)
    rgb_contrast = (rgb_contrast * 255.).astype(numpy.uint8)

    contrast_map = adaptive_thresholding(rgb_contrast, thresh_block_size, thresh_c)

    flare_bggr = Bayer2halfRGB(original_Bayer)

    # locate strong source based on intensity
    mask_for_intensitymap = (flare_bggr > flare_thres).astype(numpy.float32)
    intensity_map = numpy.clip(numpy.sum(mask_for_intensitymap, axis=2), 0, 1)
    point_src_map = contrast_map * intensity_map

    psf_conv_source = psf_conv(rgb, point_src_map, gray_half_psf, psf_fr)

    # Dilation step
    psf_conv_thresh = (psf_conv_source > psf_conv_thresh).astype(
        numpy.float32)  # thresholding the values obtained after convolving PSF and point light source
    kernel = numpy.ones((psf_blur_ksize, psf_blur_ksize), numpy.uint8)
    psf_dilated_bin = cv2.dilate(psf_conv_thresh, kernel)

    # load portrait mask

    if is_mask == True:  # use portrait mask to protect
        face = numpy.float16(mask)
        face2 = numpy.ones(face.shape)
        face2[numpy.where(face > 0.3)] = 0
        psf_dilated_bin *= face2  # exclude face region

    # smoothing on the binary mask for smoother blend
    mask_d = cv2.GaussianBlur(psf_dilated_bin, (smooth_ksize, smooth_ksize), smooth_sigma, cv2.BORDER_DEFAULT)

    ## Lines below are copied from M0 method
    # binarize the mask
    weight_thres = 0.1
    mask_d2 = numpy.zeros(mask_d.shape)
    mask_d2[numpy.where(mask_d > weight_thres)] = 1

    # find the contours  [group the connected regions]
    contours, hierarchy = cv2.findContours(numpy.uint8(mask_d2 * 255), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2:]

    L = len(contours)  # number of connected regions

    # generate bounding box for each connected region, for patch based convoluton
    mask_label_2 = numpy.zeros(mask_d2.shape)
    for idx in range(L):
        X, Y = get_edge_points_cv(contours, idx)  # X,Y indicate the edges of the region
        mask_label_2[X, Y] = (idx + 1)

    return mask_d, mask_label_2


