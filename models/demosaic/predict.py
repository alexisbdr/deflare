#!/usr/bin/env python
""""""
import os

import cv2
import numpy as np
import torch
import skimage.io as skio

from . import modules 
from . import converter
from .utils import *

def predict_rgb_from_bayer_tensor(im,model,pattern,device):
    '''
        predict the RGB imgae from bayer pattern mosaic using demosaic net

    :param im:
        [batch_sz, 1, h,w] tensor. the bayer pattern mosiac.
    :param model:
        the NN model (demosaic_net)
    :param RGGB:
        the cfa layout. the demosaic net is trained w/ GRBG. If the input is RGGB,
        we need to crop the last and first column before feeding to the demosaic net
    :param device:
        "cuda:0" or "cpu"
    :return:
        rgb_hat:
          [batch_size, 3, m,n]  the rgb image predicted by the demosaic-net using our bayer input
    '''
    # max_thre=2**8-1

    batch_size = im.shape[0]
    n_channel = im.shape[1]
    if n_channel==1:            # gray scale image
        im= torch.cat((im, im, im), 1)

                      # to make sure the r,g,b, channel dimension the same
    # max_thre=2**8-1
    # if np.max(im)>1:
    #     im = im.astype(np.float32) / max_thre  # normalize the image by 255

    #convert BGGR to GRBG
    if pattern=='BGGR':
        im=im[:,:,1:-1,:]
    else : #RGGB
        im = im[:, :, :, 1:-1]
    
    im= bayer_mosaic_tensor(im,device)                                 # conver the RGB image to bayer mosaic

    sample = {"mosaic": im}

    rgb_hat = model(sample)

    rgb_hat = torch.clamp(rgb_hat, min=0, max=1)

    return rgb_hat

# predict rgb from bayer pattern (RGGB).
# the demosaic-net is trained using GRGB. Therefore, the output RGB image will be 2 pixel narrower than the input bayer pattern.
# [in]
#   im:         ndarray [m,n]
#   model:      demosaic_net
#   device:     e.g. "cuda:0"
#
#[out]
#   out_ref:   [m,n-2,3] ndarray in the range of [0,1] in RGB order.
#               If saved with cv2.imwrite, need to convert from RGB to BGR before saving.
#
def predict_rgb_from_bayer(im,model,device):

    max_thre=2**8-1

    if len(im.shape)==2:            # gray scale image
        im= np.concatenate((im[:, :, np.newaxis], im[:, :, np.newaxis], im[:, :, np.newaxis]), axis=2)

    im = im[:,1:-1]                                     # our bayer pattern is RGGB. but the model is trained by GRBG. Need to crop the first and last column
    im = im[0:-2,:]
    # im = modcrop(im, 2)                                 # to make sure the r,g,b, channel dimension the same


    if np.max(im)>1:
        im = im.astype(np.float32) / max_thre  # normalize the image by 255

    im = np.transpose(im, [2, 0, 1])                            # transpose the image dimension from [m,n,3] to [3,m,n]

    im, mask = bayer_mosaic(im)                                 # conver the RGB image to bayer mosaic

    im = np.expand_dims(im, 0)                                  # add 1 to the first dim. the shape of im changes from [3,m,n] to [1,3,m,n]
    im = torch.from_numpy(im).to(device)                        # convert to tensor

    sample = {"mosaic": im}

    outr = model(sample)

    out_ref = outr[0]

    ####################################                    post processing of model output (from GPU to CPU) and save the results                   ###################################3

    out_ref = out_ref.permute(1, 2, 0).cpu().numpy()            # reference RGB image (predicted by the pretrained model)
    out_ref = np.clip(out_ref, 0, 1)

    return out_ref

def get_demosaic_net_model(pretrained,device):

    model_ref = modules.get({"model": "BayerNetwork"})  # load model coefficients if 'pretrained'=True
    cvt = converter.Converter(pretrained, "BayerNetwork")
    cvt.convert(model_ref)
    for p in model_ref.parameters():
        p.requires_grad = False
    model_ref.to(device)

    return model_ref

###########################################################3                                main function to evaluate the NN model

def main(data_dir,pretrained,output,device,fix_seed):

    if fix_seed:                                                                       # to make sure the generated random number is reproducible
        np.random.seed(0)
        torch.manual_seed(0)

    #
    # model_ref = modules.get({"model": "BayerNetwork"})                              # load model coefficients if 'pretrained'=True
    # cvt = converter.Converter(pretrained, "BayerNetwork")
    # cvt.convert(model_ref)
    # for p in model_ref.parameters():
    #     p.requires_grad = False
    # model_ref.to(device)

    model_ref = get_demosaic_net_model(pretrained,device)

    for path in os.listdir(data_dir):

        file_ext = os.path.splitext(path)[-1]
        if not file_ext == ".png":                                                #only work on tif file
            continue

        print(path)

        #  1  cv2.IMREAD_COLOR : Loads a color image. Any transparency of image will be neglected. It is the default flag.
        #  0  cv2.IMREAD_GRAYSCALE : Loads image in grayscale mode
        # -1  cv2.IMREAD_UNCHANGED : Loads image as such including alpha channel
        im = cv2.imread(os.path.join(data_dir, path), 0)              # the demosiac image is [m,n]. but when read by cv2.imread, it is [m,n,3]. The three color channel has the same value

        out_ref=predict_rgb_from_bayer(im,model_ref,device)
        dst_path = os.path.join(output, path.replace(file_ext, "_ref2.png"))  # save as png
        skio.imsave(dst_path, out_ref)


        pass

