import numpy as np
import torch

def modcrop_square(img, modulo):

    h = img.shape[0]
    w = img.shape[1]

    sz=np.min([h,w])
    sz= (sz//modulo)*modulo

    if len(img.shape) == 3:
        img = img[0:sz, 0:sz, :]
    else:
        img = img[0:sz, 0:sz]

    return img

def modcrop(img, modulo):

    h=img.shape[0]
    w=img.shape[1]

    # sz=np.min([h,w])
    # sz= (sz//modulo)*modulo


    h= (h//modulo)*modulo
    w= (w//modulo)*modulo

    h = int(h)
    w = int(w)
    sz = np.asarray([h,w])


    if len(img.shape) == 3:
        img = img[0:sz[0],0:sz[1],:]
    else:
        img = img[0:sz[0], 0:sz[1]]

    return img


def modcrop_tensor(img, modulo):

    h=img.shape[2]
    w=img.shape[3]

    n_channel = img.shape[1]

    sz=np.min([h,w])

    sz= (sz//modulo)*modulo


    img = img[:,:,0:sz,0:sz]

    return img

#[summary]
#   convert a RGB input image to bayer mosaic and mask
#[in]
#   im: input image. [3,m,n] ndarray
#
#[out]
#   bayer pattern of GR of the image. [3,m,n]
#                    BG
#
#   mask: mask of GR. [3,m,n]
#                 BG
#
def bayer_mosaic(im):
    """GRBG Bayer mosaic."""

    mos = np.copy(im)
    mask = np.ones_like(im)                 # create a ndarray of size 'im' filled with '1's

    # red
    mask[0, ::2, 0::2] = 0
    mask[0, 1::2, :] = 0

    # green
    mask[1, ::2, 1::2] = 0
    mask[1, 1::2, ::2] = 0

    # blue
    mask[2, 0::2, :] = 0
    mask[2, 1::2, 1::2] = 0

    return mos*mask, mask


# [in]
#   im: [batch_size, 3, m,n]. The color is in RGB order.
#
def bayer_mosaic_tensor(im,device):
    """GRBG Bayer mosaic."""

    batch_size=im.shape[0]
    hh=im.shape[2]
    ww=im.shape[3]

    mask = torch.ones([batch_size,3,hh, ww], dtype=torch.float32, device=device)

    # red
    mask[:,0, ::2, 0::2] = 0
    mask[:,0, 1::2, :] = 0

    # green
    mask[:,1, ::2, 1::2] = 0
    mask[:,1, 1::2, ::2] = 0

    # blue
    mask[:,2, 0::2, :] = 0
    mask[:,2, 1::2, 1::2] = 0

    return im*mask

def crop_like(src, tgt):
  src_sz = np.array(src.shape)
  tgt_sz = np.array(tgt.shape)
  crop = (src_sz[2:4]-tgt_sz[2:4]) // 2
  if (crop > 0).any():
    return src[:, :, crop[0]:src_sz[2]-crop[0], crop[1]:src_sz[3]-crop[1], ...]
  else:
    return src