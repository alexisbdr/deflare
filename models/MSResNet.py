import torch
import torch.nn as nn

from . import common
from .ResNet import ResNet
from .MBCNN.blocks import PreBlock, FTRB
from .MBCNN.layers import LBFLayer
from utils import gaussian_pyramid


class conv_end(nn.Module):
    def __init__(self, in_channels=3, out_channels=3, kernel_size=5, ratio=2):
        super(conv_end, self).__init__()

        modules = [
            common.default_conv(in_channels, out_channels, kernel_size),
            nn.PixelShuffle(ratio)
        ]

        self.uppath = nn.Sequential(*modules)

    def forward(self, x):
        return self.uppath(x)


class MSResNet(nn.Module):
    def __init__(self,
        n_feats: int,
        kernel_size: int,
        n_resblocks: int,
        n_scales: int):
        super(MSResNet, self).__init__()

        self.mean = .5

        self.n_resblocks = n_resblocks
        self.n_feats = n_feats
        self.kernel_size = kernel_size

        self.n_scales = n_scales

        self.body_models = nn.ModuleList([
            ResNet(3, 3, n_feats, kernel_size, 
            n_resblocks, mean_shift=False),
        ])
        for _ in range(1, self.n_scales):
            self.body_models.insert(0, 
                ResNet(6, 3, n_feats, kernel_size, 
                n_resblocks, mean_shift=False))

        self.conv_end_models = nn.ModuleList([None])
        for _ in range(1, self.n_scales):
            self.conv_end_models += [conv_end(3, 12)]

    def forward(self, input_pyramid):

        scales = range(self.n_scales-1, -1, -1)    # 0: fine, 2: coarse

        for s in scales:
            input_pyramid[s] = input_pyramid[s]

        output_pyramid = [None] * self.n_scales

        input_s = input_pyramid[-1]
        for s in scales:    # [2, 1, 0]
            output_pyramid[s] = self.body_models[s](input_s)
            if s > 0:
                up_feat = self.conv_end_models[s](output_pyramid[s])
                input_s = torch.cat((input_pyramid[s-1], up_feat), 1)

        for s in scales:
            output_pyramid[s] = output_pyramid[s]

        return output_pyramid


class MSResNetLBF(nn.Module):
    def __init__(self,
        n_feats: int,
        kernel_size: int,
        n_resblocks: int,
        n_scales: int,
        p_block: int):
        
        super(MSResNetLBF, self).__init__()

        self.n_resblocks = n_resblocks
        self.n_feats = n_feats
        self.kernel_size = kernel_size

        self.n_scales = n_scales

        self.body_models = nn.ModuleList([
            nn.Sequential(
                LBFLayer(3, 3, p_block),
                ResNet(3, 3, n_feats, kernel_size, 
                n_resblocks, mean_shift=False)
            )
        ])
        for _ in range(1, self.n_scales):
            self.body_models.insert(0, 
                nn.Sequential(
                    LBFLayer(6, 6, p_block),
                    ResNet(6, 3, n_feats, kernel_size, 
                    n_resblocks, mean_shift=False)
                    )
                )

        self.conv_end_models = nn.ModuleList([None])
        for _ in range(1, self.n_scales):
            self.conv_end_models += [conv_end(3, 12)]
    
    def forward(self, input_pyramid):
        
        scales = range(self.n_scales-1, -1, -1)    # 0: fine, 2: coarse

        for s in scales:
            input_pyramid[s] = input_pyramid[s]

        output_pyramid = [None] * self.n_scales

        input_s = input_pyramid[-1]
        for s in scales:    # [2, 1, 0]
            output_pyramid[s] = self.body_models[s](input_s)
            if s > 0:
                up_feat = self.conv_end_models[s](output_pyramid[s])
                input_s = torch.cat((input_pyramid[s-1], up_feat), 1)

        for s in scales:
            output_pyramid[s] = output_pyramid[s]

        return output_pyramid


class MSResNetFTRB(nn.Module):
    def __init__(self,
        in_channels: int,
        n_feats: int,
        kernel_size: int,
        n_resblocks: int,
        n_scales: int,
        p_block: int):
        
        super(MSResNetFTRB, self).__init__()

        self.n_resblocks = n_resblocks
        self.n_feats = n_feats
        self.kernel_size = kernel_size

        self.n_scales = n_scales
        
        # TODO: parametrize in terms of n_scales
        d_list = [(1, 2, 3, 2, 1),
            (1, 2, 3, 2, 1),
            (1, 2, 2, 2, 1)]

        self.body_models = nn.ModuleList([
            nn.Sequential(
                FTRB(in_channels, in_channels, d_list.pop(-1), p_block), 
                ResNet(in_channels, in_channels, n_feats, kernel_size, 
                n_resblocks, mean_shift=False)
            )
        ])
        for _ in range(1, self.n_scales):
            self.body_models.insert(0, 
                nn.Sequential(
                    FTRB(in_channels*2, in_channels*2, d_list.pop(-1), p_block),
                    ResNet(in_channels*2, in_channels, n_feats, kernel_size, 
                    n_resblocks, mean_shift=False)
                    )
            )

        self.conv_end_models = nn.ModuleList([None])
        for _ in range(1, self.n_scales):
            self.conv_end_models += [conv_end(in_channels, in_channels*4, kernel_size=self.kernel_size)]

    def forward(self, input_pyramid):

        scales = range(self.n_scales-1, -1, -1)    # 0: fine, 2: coarse

        for s in scales:
            input_pyramid[s] = input_pyramid[s]

        output_pyramid = [None] * self.n_scales

        input_s = input_pyramid[-1]
        for s in scales:    # [2, 1, 0]
            output_pyramid[s] = self.body_models[s](input_s)
            if s > 0:
                up_feat = self.conv_end_models[s](output_pyramid[s])
                input_s = torch.cat((input_pyramid[s-1], up_feat), 1)

        for s in scales:
            output_pyramid[s] = output_pyramid[s]

        return output_pyramid


def test_MSResNetLBF():
    
    device = 'cuda' if torch.cuda.is_available() else 'cpu' 
    _in = torch.zeros((2, 3, 224, 224))
    _in = _in.to(device)
    model_64 = MSResNetLBF(64, 5, 18, 1, 8)
    model_64 = model_64.to(device)
    model_32 = MSResNetLBF(32, 5, 18, 1, 8)
    model_32 = model_32.to(device)
    res_64 = model_64(_in)
    print(f"Res 64 {res_64.shape}")
    res_32 = model_32(_in)
    print(f"Res 32 {res_32.shape}")

def test_MSResNetFTRB():
    
    device = 'cuda' if torch.cuda.is_available() else 'cpu' 
    _in = torch.zeros((2, 4, 224, 224))
    _in = _in.to(device)
    _in = gaussian_pyramid(_in, 3)
    model_64 = MSResNetFTRB(_in[0].shape[1], 64, 5, 18, 3, 8)
    model_64 = model_64.to(device)
    model_32 = MSResNetFTRB(_in[0].shape[1], 32, 5, 18, 3, 8)
    model_32 = model_32.to(device)
    res_64 = model_64(_in)
    print(f"Res 64 {res_64.shape}")
    res_32 = model_32(_in)
    print(f"Res 32 {res_32.shape}")

if __name__ == "__main__":
    test_MSResNetFTRB()