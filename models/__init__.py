from models.MSResNet import MSResNet, MSResNetLBF, MSResNetFTRB
from models.MBCNN import MBCNN
from models.UNet import UNetDynamic
from models.deflare.Lightweight_model import ResSkip_arch
from models.deflare.deflare import Deflare
from models.demosaic import get_demosaic_net_model, predict_rgb_from_bayer_tensor

model_map = {
	"MSResNet": MSResNet,
	"MBCNN": MBCNN,
	"UNet": UNetDynamic
}

def load_model(config, model_name: str):
	"""Acts as a factory function for the different models
	"""

	if model_name == "MSResNet": 
		config = config["MSResNet"]
		model = MSResNet(
			config["n_feats"], config["kernel_size"],
			config["n_resblocks"], config["n_scales"]
		)
	elif model_name == "MBCNN": 
		config = config["MBCNN"]
		model = MBCNN(3, config["depth"], config["p_block"], multi=True)
	elif model_name == "UNet": 
		config = config["UNet"]
		model = UNetDynamic(
			3, 3, config["depth"],
			config["layers"], config["bilinear"] == "True",
			config["batch_norm"] == "True"
		)
	elif model_name == "MSResNetLBF":
		config = config["MSResNetLBF"]
		model = MSResNetLBF(
			config["n_feats"], config["kernel_size"],
			config["n_resblocks"], config["n_scales"],
			config["p_block"]
		)
	elif model_name == "MSResNetFTRB":
		config = config["MSResNetFTRB"]
		model = MSResNetFTRB(
			config["in_channels"], config["n_feats"], config["kernel_size"],
			config["n_resblocks"], config["n_scales"],
			config["p_block"]
		)
	elif model_name == "deflare":
		#Shuai's deflare module
		#For prediction only
		model = None
		config = config["deflare"]
	elif model_name == "ResSkipC":
		config = config["ResSkipC"]	
		model = ResSkip_arch.ResSkipC_l(3, config["channels"]) 

	return model, config